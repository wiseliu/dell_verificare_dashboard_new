angular.module("Dashboard")
        .controller("BrowseDistributorsController", function ($window, $location, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {

                $scope.Data = Data;
                $scope.Data.link = 'browsedistributors';
                $scope.Data.parent = 'stores';

                var controller = this;

                $scope.go = function (path) {
                    $location.path($location.path() + path);
                };

                $scope.loading = true;

                var controller = this;

                var vm = this;
                vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/distributors/list',
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    }
                    var defer = $q.defer();
                    $http(req).then(function (data) {
                        controller.response = data.data;

                        $scope.distributors = controller.response.data;
                        $scope.loading = false;
                        defer.resolve($scope.distributors);
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {
                            $scope.distributors = '';
                            $scope.loading = false;
                            $scope.defer.resolve($scope.distributors);
                        }else {

                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                    return defer.promise;
                }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting',[]);

                vm.dtColumns = [
                    DTColumnBuilder.newColumn('id').withTitle('ID'),
                    DTColumnBuilder.newColumn('store_uid').withTitle('Unique ID'),
                    DTColumnBuilder.newColumn('name').withTitle('Name'),
                    DTColumnBuilder.newColumn('store_building').withTitle('Building Name'),
                    DTColumnBuilder.newColumn('phone').withTitle('Phone'),
                    DTColumnBuilder.newColumn('address').withTitle('Address'),
                    DTColumnBuilder.newColumn('city.name').withTitle('City'),
                    DTColumnBuilder.newColumn('email').withTitle('Email')
                ];
            }
            
            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });
