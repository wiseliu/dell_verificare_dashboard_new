angular.module("Dashboard")
        .controller("CompetitorIssuesController", function ($window, $location, $filter, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'competitorissues';
                $scope.Data.parent = 'reports';

                var controller = this;

                $scope.go = function (path) {
                    $location.path($location.path() + path);
                };

                var vm = this;
                vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    var defer = $q.defer();
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/issues/list',
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    }

                    $http(req).then(function (data) {
                        controller.response = data.data;

                        $scope.issues = controller.response.data;
                        $scope.loading = false;
                        defer.resolve($scope.issues);

                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {
                            $scope.issues = {};
                            defer.resolve($scope.issues);
                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                    return defer.promise;
                }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting',[]);

                vm.dtColumns = [
                    DTColumnBuilder.newColumn('id').withTitle('ID'),
                    DTColumnBuilder.newColumn('program_name').withTitle('Program Name'),
                    DTColumnBuilder.newColumn('brand_name').withTitle('Brand Name'),
                    DTColumnBuilder.newColumn('store_name').withTitle('Store Name'),
                    DTColumnBuilder.newColumn('campaign_start').withTitle('Campaign Start').renderWith(function (data, type, full) {
                        return $filter('date')(data, "dd/MM/yyyy HH:mm");
                    }),
                    DTColumnBuilder.newColumn('campaign_end').withTitle('Campaign End').renderWith(function (data, type, full) {
                        return $filter('date')(data, "dd/MM/yyyy HH:mm");
                    }),
                    DTColumnBuilder.newColumn('remark').withTitle('Remark'),
                    DTColumnBuilder.newColumn('photo_name.photo_name.url').withTitle('Image').renderWith(function (data, type, full) {
                        return "<a href='"+ full.photo_name.photo_name.url + "'><img class='tableimg' src='" + full.photo_name.photo_name.url + "' alt='Competitor image'></img></a>";
                    })
                ];
            }
            
            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });