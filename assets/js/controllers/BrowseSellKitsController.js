angular.module("Dashboard")
        .controller("BrowseSellKitsController", function ($window, $location, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browsesellkits';
                $scope.Data.parent = 'sellkits';

                var controller = this;
                $scope.level = $localStorage.level;

                $scope.go = function (path) {
                    $location.path($location.path() + path);
                };

                var vm = this;
                vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    var defer = $q.defer();
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/sell_kits/list_dashboard',
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    }

                    $http(req).then(function (data) {
                        controller.response = data.data;

                        $scope.sellkits = controller.response.data;
                        $scope.loading = false;
                        defer.resolve($scope.sellkits);

                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {
                            $scope.sellkits = '';
                            defer.resolve($scope.sellkits);
                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                    return defer.promise;
                }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting',[]);

                    vm.dtColumns = [
                        DTColumnBuilder.newColumn('id').withTitle('ID'),
                        DTColumnBuilder.newColumn('name').withTitle('Name'),
                        DTColumnBuilder.newColumn('description').withTitle('Description'),
                        DTColumnBuilder.newColumn('file_name.file_name.url').withTitle('File').renderWith(function (full) {
                            return ' <a class="btn btn-success btn-small" href="' + full + '"> View </a>';
                        }),
                        DTColumnBuilder.newColumn('category').withTitle('Category'),
                        DTColumnBuilder.newColumn('family').withTitle('Family')
                    ];
            }
            
            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });