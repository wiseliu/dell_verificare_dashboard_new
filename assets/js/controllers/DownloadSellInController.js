angular.module("Dashboard")
        .controller("DownloadSellInController", function ($window, $scope, Data, $http, $localStorage) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'downloadsellin';
                $scope.Data.parent = 'sellins';

                $scope.waiting = true;
                $scope.success = false;

                $scope.download = function () {
                    if (angular.isUndefined($scope.quarter_year_from)
                            || angular.isUndefined($scope.quarter_from)
                            || angular.isUndefined($scope.quarter_week_from)
                            || $scope.quarter_year_from === null
                            || $scope.quarter_from === null
                            || $scope.quarter_week_from === null
                            || angular.isUndefined($scope.quarter_year_to)
                            || angular.isUndefined($scope.quarter_to)
                            || angular.isUndefined($scope.quarter_week_to)
                            || $scope.quarter_year_to === null
                            || $scope.quarter_to === null
                            || $scope.quarter_week_to === null
                            ) {
                        $scope.fail = true;
                        $scope.errmsg = "Please fill all fields";
                    } else if ($scope.quarter_year_to < 2012 || $scope.quarter_year_from < 2012) {
                        $scope.fail = true;
                        $scope.errmsg = "Please enter a valid year (2012+)";
                    } else {
                        $scope.process = true;
                        $scope.fail = false;

                        var req = {
                            method: 'GET',
                            url: 'https://dell-api.verifi.care/sellins/export?'
                                    + 'quarter_year_from=' + $scope.quarter_year_from
                                    + '&quarter_from=' + $scope.quarter_from
                                    + '&quarter_week_from=' + $scope.quarter_week_from
                                    + '&quarter_year_to=' + $scope.quarter_year_to
                                    + '&quarter_to=' + $scope.quarter_to
                                    + '&quarter_week_to=' + $scope.quarter_week_to
                            ,
                            headers: {
                                'Authorization': $localStorage.jwt
                            }
                        };

                        $http(req).then(function (data) {

                            var blob = new Blob([data.data], {type: 'text/csv'});
                            var filename = 'sellins_' + $scope.quarter_year_from + '_Q' + $scope.quarter_from + '_QWeek' + $scope.quarter_week_from + '.csv';
                            if (window.navigator.msSaveOrOpenBlob) {
                                window.navigator.msSaveBlob(blob, filename);
                            } else {
                                var elem = window.document.createElement('a');
                                elem.href = window.URL.createObjectURL(blob);
                                elem.download = filename;
                                document.body.appendChild(elem);
                                elem.click();
                                document.body.removeChild(elem);
                            }

//                            var anchor = angular.element('<a/>');
//                            anchor.css({display: 'none'}); // Make sure it's not visible
//                            angular.element(document.body).append(anchor); // Attach to document
//
//                            anchor.attr({
//                                href: 'data:attachment/csv;charset=utf-8,' + encodeURI(data.data),
//                                target: '_blank',
//                                download: 'sellins_' + $scope.quarter_year_from + '_Q' + $scope.quarter_from + '_QWeek' + $scope.quarter_week_from + '.csv'
//                            })[0].click();
//
//                            anchor.remove();

                            $scope.process = false;
                            $scope.waiting = false;
                            $scope.success = true;
                        }, function (data) {
                            if (data.status === 401) {
                                alert("Your credential is expired or invalid, please log in again");
                                $localStorage.$reset();
                                $window.location.href = 'login.html';
                            }
                            $scope.fail = true;

                            $scope.errmsg = "An error has occurred, please try again later";

                            $scope.waiting = true;
                            $scope.process = false;
                        });
                    }
                }
            }

            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });