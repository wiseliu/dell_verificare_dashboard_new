angular.module("Dashboard")
        .controller("AddCAMController", function ($window, $http, $scope, $localStorage, Data) {
            if ($localStorage.level !== 'admin') {
                $window.location.href = $window.location.href + '/../../dashboard';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'addcam';
                $scope.Data.parent = 'users';

                $scope.waiting = true;
                $scope.success = false;

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/managers/list',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.managers = data.data.data;
                }, function (data) {

                });

                $scope.addCAM = function () {
                    $scope.process = true;
                    if (angular.isUndefined($scope.parent_id)) {
                        var req = {
                            method: 'POST',
                            url: 'https://dell-api.verifi.care/managers/create',
                            headers: {
                                'Authorization': $localStorage.jwt
                            },
                            data: {
                                name: $scope.name,
                                level: 0
                            }
                        };
                    } else {
                        var req = {
                            method: 'POST',
                            url: 'https://dell-api.verifi.care/managers/create',
                            headers: {
                                'Authorization': $localStorage.jwt
                            },
                            data: {
                                name: $scope.name,
                                level: 0,
                                parent_id: $scope.parent_id
                            }
                        };
                    }

                    $http(req).then(function (data) {
                        $scope.process = false;
                        $scope.success = true;
                        $scope.waiting = false;
                        $scope.fail = false;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        }
                        $scope.fail = true;
                        $scope.process = false;
                    });
                };

                $scope.reset = function () {
                    $scope.level = "";
                    $scope.parent_id = "";
                    $scope.name = "";

                    $scope.process = false;
                    $scope.waiting = true;
                    $scope.success = false;
                };
            }

            function injectScript() {
                $('select').select2();
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });