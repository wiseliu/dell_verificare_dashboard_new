angular.module("Dashboard")
        .controller("SearchSellOutController", function ($filter, $routeParams, $window, $location, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            $scope.Data = Data;
            $scope.Data.link = 'searchsellout';
            $scope.Data.parent = 'sellouts';
            $scope.filtered = false;
            $scope.loading = true;

            if ($localStorage.level === 'admin' || $localStorage.level === 'dashboard') {
                $scope.isAdmin = true;
            } else {
                $scope.isAdmin = false;
            }

            $scope.service_tag_filter = $routeParams.filter;

            var controller = this;
            if (angular.isUndefined($scope.service_tag_filter)) {
                $scope.filtered = false;
            } else {
                if ($scope.service_tag_filter.indexOf(',') > -1) {
                    url = 'https://dell-api.verifi.care/sellouts/bulk_search?q=' + $scope.service_tag_filter;
                } else {
                    url = 'https://dell-api.verifi.care/sellouts/search?q=' + $scope.service_tag_filter;
                }
                $scope.filtered = true;
                var vm = this;
                vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    var req = {
                        method: 'GET',
                        url: url,
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    }
                    $scope.defer = $q.defer();
                    $http(req).then(function (data) {
                        controller.response = data.data;

                        $scope.sellouts = controller.response.data;
                        $scope.loading = false;
                        $scope.defer.resolve($scope.sellouts);
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {
                            $scope.sellouts = '';
                            $scope.defer.resolve($scope.sellouts);
                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                    return $scope.defer.promise;
                }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting', []);

                if ($scope.isAdmin) {
                    vm.dtColumns = [
                        DTColumnBuilder.newColumn('id').withTitle('ID'),
                        DTColumnBuilder.newColumn('service_tag').withTitle('Service Tag'),
                        DTColumnBuilder.newColumn('updated_at').withTitle('Updated At').renderWith(function (data, type, full) {
                            return $filter('date')(data, "dd/MM/yyyy HH:mm");
                        }),
                        DTColumnBuilder.newColumn('store.store_uid').withTitle('Store Uid'),
                        DTColumnBuilder.newColumn('store.name').withTitle('Store Name'),
                        DTColumnBuilder.newColumn('user.username').withTitle('Username'),
                        DTColumnBuilder.newColumn('user.name').withTitle('User Name'),
                        DTColumnBuilder.newColumn('proof.proof.url').withTitle('Proof').renderWith(function (data, type, full) {
                            return "<a href='" + full.proof.proof.url + "'><img class='tableimg' src='" + full.proof.proof.url + "' alt='Proof image'></img></a>";
                        }),
                        DTColumnBuilder.newColumn('id').withTitle('Edit').renderWith(function (full) {
                            return ' <a class="btn btn-success btn-small" href="/sellouts/browse-sell-out/' + full + '"> Edit Proof </a>';
                        })
                    ];
                } else {
                    vm.dtColumns = [
                        DTColumnBuilder.newColumn('id').withTitle('ID'),
                        DTColumnBuilder.newColumn('service_tag').withTitle('Service Tag'),
                        DTColumnBuilder.newColumn('updated_at').withTitle('Updated At').renderWith(function (data, type, full) {
                            return $filter('date')(data, "dd/MM/yyyy HH:mm");
                        }),
                        DTColumnBuilder.newColumn('store.store_uid').withTitle('Store Uid'),
                        DTColumnBuilder.newColumn('store.name').withTitle('Store Name'),
                        DTColumnBuilder.newColumn('user.username').withTitle('Username'),
                        DTColumnBuilder.newColumn('user.name').withTitle('User Name'),
                        DTColumnBuilder.newColumn('proof.proof.url').withTitle('Proof').renderWith(function (data, type, full) {
                            return "<a href='" + full.proof.proof.url + "'><img class='tableimg' src='" + full.proof.proof.url + "' alt='Proof image'></img></a>";
                        })
                    ];
                }
            }

            $scope.filterSearch = function () {
                if (!angular.isUndefined($scope.service_tag_filter)) {
                    $location.path('sellouts/search-sell-out/' + $scope.service_tag_filter);
                }
            }

            function injectScript() {

            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });
