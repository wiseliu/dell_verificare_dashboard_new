angular.module("Dashboard")
        .controller("AddInventoryController", function ($window, $http, $scope, $localStorage, Data) {
            if ($localStorage.level !== 'admin') {
                $window.location.href = $window.location.href + '/../../dashboard';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'addinventory';
                $scope.Data.parent = 'stocks';

                $scope.waiting = true;
                $scope.success = false;



                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/stores/list',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.stores = data.data.data;
                }, function (data) {

                });

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/users/list',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.users = data.data.data;
                }, function (data) {

                });

                $scope.addInventory = function () {
                    $scope.process = true;

                    var req = {
                        method: 'POST',
                        url: 'https://dell-api.verifi.care/inventories/create',
                        headers: {
                            'Authorization': $localStorage.jwt
                        },
                        data: {
                            service_tag: $scope.servtag,
                            store_id: $scope.store_id,
                            added_by: $scope.user_id
                        }
                    };

                    $http(req).then(function (data) {
                        $scope.process = false;
                        $scope.success = true;
                        $scope.waiting = false;
                        $scope.fail = false;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        }
                        $scope.fail = true;
                        $scope.process = false;
                        $scope.errmsg = "An error has occurred, please try again later";
                    });
                };

                $scope.reset = function () {
                    $scope.level = "";
                    $scope.parent_id = "";
                    $scope.name = "";

                    $scope.process = false;
                    $scope.waiting = true;
                    $scope.success = false;
                };
            }

            function injectScript() {
                $('select').select2();
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });