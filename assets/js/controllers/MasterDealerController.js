angular.module("Dashboard")
        .controller("MasterDealerController", function ($window, $scope, Data) {
            $scope.Data = Data;
            $scope.Data.link = 'masterdealer';
            $scope.Data.parent = 'reports';

            function injectScript() {
                $('.card-table-IoneTable').cardtable();
                $(".select2load").select2();
                console.clear();
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });