angular.module("Dashboard")
        .controller("SearchVisibilityController", function ($filter, $routeParams, $window, $location, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'searchvisibility';
                $scope.Data.parent = 'visibilities';
                $scope.filtered = false;
                $scope.loading = true;

                $scope.user_id_filter = $routeParams.user_id;
                $scope.store_id_filter = $routeParams.store_id;

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/users/list',
                    headers: {
                        'Content-Type': "application/x-www-form-urlencoded",
                        'Authorization': $localStorage.jwt
                    }
                }

                $http(req).then(function (data) {
                    $scope.users = data.data.data;

                }, function (data) {

                });

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/stores/list',
                    headers: {
                        'Content-Type': "application/x-www-form-urlencoded",
                        'Authorization': $localStorage.jwt
                    }
                }

                $http(req).then(function (data) {
                    $scope.stores = data.data.data;

                    $('select').select2();
                }, function (data) {
                    $scope.stores = [];
                });

                var controller = this;
                if (angular.isUndefined($scope.user_id_filter) || angular.isUndefined($scope.store_id_filter)) {
                    $scope.filtered = false;
                } else {
                    $scope.filtered = true;
                    var vm = this;
                    vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                        var req = {
                            method: 'GET',
                            url: 'https://dell-api.verifi.care/visibilities/list_per_user_store?store_id=' + $scope.store_id_filter + '&user_id=' + $scope.user_id_filter,
                            headers: {
                                'Content-Type': "application/x-www-form-urlencoded",
                                'Authorization': $localStorage.jwt
                            }
                        }
                        $scope.defer = $q.defer();
                        $http(req).then(function (data) {
                            controller.response = data.data;

                            $scope.visibilities = controller.response.data;
                            $scope.loading = false;
                            $scope.defer.resolve($scope.visibilities);
                        }, function (data) {
                            if (data.status === 401) {
                                alert("Your credential is expired or invalid, please log in again");
                                $localStorage.$reset();
                                $window.location.href = 'login.html';
                            } else if (data.status === 404) {
                                $scope.visibilities = '';
                                $scope.loading = false;
                                $scope.defer.resolve($scope.visibilities);
                            } else {

                            }
                            $scope.loading = false;
                            $scope.error = true;
                        });
                        return $scope.defer.promise;
                    }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting',[]);

                    vm.dtColumns = [
                        DTColumnBuilder.newColumn('id').withTitle('ID'),
                        DTColumnBuilder.newColumn('visibility.visibility.url').withTitle('Image').renderWith(function (data, type, full) {
                            return "<a href='" + full.visibility.visibility.url + "'>" + full.visibility.visibility.url + "</a>";
                        }),
                        DTColumnBuilder.newColumn('remark').withTitle('Remark'),
                        DTColumnBuilder.newColumn('category').withTitle('Category'),
                        DTColumnBuilder.newColumn('store.store_uid').withTitle('Store UID'),
                        DTColumnBuilder.newColumn('user.username').withTitle('Username'),
                        DTColumnBuilder.newColumn('created_at').withTitle('Created At').renderWith(function (data, type, full) {
                            return $filter('date')(data, "dd/MM/yyyy HH:mm");
                        })
                    ];
                }

                $scope.filterSearch = function () {
                    if (!(angular.isUndefined($scope.user_id_filter) || angular.isUndefined($scope.store_id_filter))) {
                    $location.path('visibilities/search-visibility/' + $scope.user_id_filter + '/' + $scope.store_id_filter);
                    }
                }
            }

            function injectScript() {

            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });
