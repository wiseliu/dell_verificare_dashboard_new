angular.module("Dashboard")
        .controller("AddUserController", function ($window, $http, $scope, $localStorage, Data) {
            if ($localStorage.level !== 'admin') {
                $window.location.href = $window.location.href + '/../../dashboard';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'adduser';
                $scope.Data.parent = 'users';

                $scope.waiting = true;
                $scope.success = false;

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/managers/list',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.managers = data.data.data;
                }, function (data) {

                });

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/users/list_level',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.levels = data.data.data.level;
                    delete $scope.levels.admin;
                }, function (data) {

                });

                $scope.addUser = function () {
                    $scope.process = true;
                    var req = {
                        method: 'POST',
                        url: 'https://dell-api.verifi.care/users/create',
                        headers: {
                            'Authorization': $localStorage.jwt
                        },
                        data: {
                            username: $scope.username,
                            password_digest: $scope.password,
                            level: $scope.level,
                            manager_id: $scope.manager,
                            name: $scope.name,
                            email: $scope.email,
                            phone: $scope.phone,
                            gender: $scope.gender
                        }
                    };

                    $http(req).then(function (data) {
                        $scope.process = false;
                        $scope.success = true;
                        $scope.fail = false;
                        $scope.waiting = false;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        }
                        $scope.fail = true;
                        $scope.process = false;
                    });
                };

                $scope.reset = function () {
                    $scope.username = "";
                    $scope.password = "";
                    $scope.level = "";
                    $scope.manager = "";
                    $scope.name = "";
                    $scope.email = "";
                    $scope.phone = "";
                    $scope.gender = "";

                    $scope.process = false;
                    $scope.waiting = true;
                    $scope.success = false;
                };
            }

            function injectScript() {
                $('select').select2();
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });