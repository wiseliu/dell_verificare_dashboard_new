angular.module("Dashboard")
        .controller("BrowsePOSMInventoriesController", function ($window, $location, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browseposminventories';
                $scope.Data.parent = 'visibilities';
                var controller = this;
                $scope.level = $localStorage.level;
                $scope.go = function (path) {
                    $location.path($location.path() + path);
                };
                $scope.loading = true;
                var vm = this;
                vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/posms/inventory/list',
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    }
                    var defer = $q.defer();
                    $http(req).then(function (data) {
                        controller.response = data.data;
                        $scope.posm = controller.response.data;
                        $scope.loading = false;
                        defer.resolve($scope.posm);
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {
                            $scope.posm = {};
                            defer.resolve($scope.posm);
                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                    return defer.promise;
                }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting',[]);
                
                vm.dtColumns = [
                    DTColumnBuilder.newColumn('id').withTitle('ID'),
                    DTColumnBuilder.newColumn('store.store_uid').withTitle('Store UID'),
                    DTColumnBuilder.newColumn('store.name').withTitle('Store Name'),
                    DTColumnBuilder.newColumn('posm.name').withTitle('POSM Name'),
                    DTColumnBuilder.newColumn('posm.category').withTitle('POSM Category'),
                    DTColumnBuilder.newColumn('quantity').withTitle('Quantity')
                ];
            }


            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });