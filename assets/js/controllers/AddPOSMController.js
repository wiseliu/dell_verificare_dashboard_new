angular.module("Dashboard")
        .controller("AddPOSMController", function ($window, $http, $scope, $localStorage, Data) {
            if ($localStorage.level !== 'admin') {
                $window.location.href = $window.location.href + '/../../dashboard';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'addposm';
                $scope.Data.parent = 'visibilities';

                $scope.waiting = true;
                $scope.success = false;
                
                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/posms/list_category',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.categories = data.data.data.level;
                    $('select').select2();
                }, function (data) {

                });

                $scope.addPOSM = function () {
                    $scope.process = true;
                    var req = {
                        method: 'POST',
                        url: 'https://dell-api.verifi.care/posms/create',
                        headers: {
                            'Authorization': $localStorage.jwt
                        },
                        data: {
                            name: $scope.name,
                            quantity: $scope.quantity,
                            category: $scope.category
                        }
                    };


                    $http(req).then(function (data) {
                        $scope.process = false;
                        $scope.success = true;
                        $scope.waiting = false;
                        $scope.fail = false;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        }
                        $scope.fail = true;
                        $scope.process = false;
                    });
                };

                $scope.reset = function () {
                    $scope.name = "";
                    $scope.quantity = "";

                    $scope.process = false;
                    $scope.waiting = true;
                    $scope.success = false;
                };
            }

            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });