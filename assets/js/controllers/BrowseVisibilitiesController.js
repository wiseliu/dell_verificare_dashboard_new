angular.module("Dashboard")
        .controller("BrowseVisibilitiesController", function ($filter, $window, $location, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browsevisibilities';
                $scope.Data.parent = 'visibilities';

                $scope.loading = true;

                var controller = this;

                var vm = this;
                vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/visibilities/list',
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    }
                    $scope.defer = $q.defer();
                    $http(req).then(function (data) {
                        controller.response = data.data;

                        $scope.visibilities = controller.response.data;
                        $scope.loading = false;
                        $scope.defer.resolve($scope.visibilities);
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        }else if (data.status === 404) {
                            $scope.visibilities = '';
                            $scope.defer.resolve($scope.visibilities);
                        } else {

                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                    return $scope.defer.promise;
                }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting',[]);

                vm.dtColumns = [
                    DTColumnBuilder.newColumn('id').withTitle('ID'),
                    DTColumnBuilder.newColumn('visibility.visibility.url').withTitle('Image').renderWith(function (data, type, full) {
                        return "<a href='" + full.visibility.visibility.url + "'><b>View Image</b></a>";
                    }),
                    DTColumnBuilder.newColumn('remark').withTitle('Remark'),
                    DTColumnBuilder.newColumn('category').withTitle('Category'),
                    DTColumnBuilder.newColumn('store.store_uid').withTitle('Store UID'),
                    DTColumnBuilder.newColumn('user.username').withTitle('Username'),
                    DTColumnBuilder.newColumn('created_at').withTitle('Created At').renderWith(function (data, type, full) {
                        return $filter('date')(data, "dd/MM/yyyy HH:mm");
                    })
                ];
            }
            function injectScript() {

            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });
