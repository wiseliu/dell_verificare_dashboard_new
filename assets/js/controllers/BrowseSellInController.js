angular.module("Dashboard")
        .controller("BrowseSellInController", function ($window, $routeParams, $scope, $location, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browsesellins';
                $scope.Data.parent = 'sellins';

                $scope.loading = true;

                $scope.page = $routeParams.page;
                if (angular.isUndefined($scope.page)) {
                    $scope.page = 1;
                }
                $scope.pageSize = 1000;                

                var controller = this;

                var vm = this;
                vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/sellins/list?p=' + $scope.page,
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    }
                    $scope.defer = $q.defer();
                    $http(req).then(function (data) {
                        controller.response = data.data;
                        
                        $scope.currentPage = $scope.page;
                        $scope.total = controller.response.totalData;
                        $scope.sellins = controller.response.data;
                        $scope.loading = false;
                        $scope.defer.resolve($scope.sellins);
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {
                            $scope.sellins = '';
                            $scope.defer.resolve($scope.sellins);
                        } else {

                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                    return $scope.defer.promise;
                }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting',[])

                vm.dtColumns = [
                    DTColumnBuilder.newColumn('id').withTitle('ID'),
                    DTColumnBuilder.newColumn('service_tag').withTitle('Service Tag'),
                    DTColumnBuilder.newColumn('quarter_year').withTitle('Quarter Year'),
                    DTColumnBuilder.newColumn('quarter').withTitle('Quarter'),
                    DTColumnBuilder.newColumn('quarter_week').withTitle('Quarter Week'),
                    DTColumnBuilder.newColumn('item_type').withTitle('Item Type'),
                    DTColumnBuilder.newColumn('part_number').withTitle('Part Number'),
                    DTColumnBuilder.newColumn('product_type').withTitle('Product Type'),
                    DTColumnBuilder.newColumn('product_name').withTitle('Product Name'),
                    DTColumnBuilder.newColumn('source_store').withTitle('Source Store'),
                    DTColumnBuilder.newColumn('target_store').withTitle('Target Store')
                ];
            }

            $scope.changepage = function (page) {
                $location.path('sellins/browse-sell-in/page/' + page);
            }
            function injectScript() {

            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });
