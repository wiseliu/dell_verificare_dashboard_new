angular.module("Dashboard")
        .controller("EditCAMController", function ($routeParams, $window, $http, $scope, $localStorage, Data) {
            if ($localStorage.level !== 'admin') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browsecams';
                $scope.Data.parent = 'users';

                $scope.waiting = true;
                $scope.success = false;

                var manager_id = $routeParams.manager_id;

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/managers/' + manager_id,
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.id = data.data.data.id;
                    $scope.name = data.data.data.name;
                    $scope.level = data.data.data.level;
                    if($scope.level === "channel_area_manager"){
                        $scope.level = 0;
                    }else if($scope.level === "area_manager"){
                        $scope.level = 1;
                    }
                    $scope.prevlevel = data.data.data.level;
                    $scope.parent_id = data.data.data.parent_id;

                    $('select').select2();
                }, function (data) {

                });

                $http(req).then(function (data) {
                    $scope.managers = data.data.data;
                }, function (data) {

                });

                $scope.editCAM = function () {
                    $scope.process = true;
                    if (angular.isUndefined($scope.parent_id)) {
                        var req = {
                            method: 'PATCH',
                            url: 'https://dell-api.verifi.care/managers/update/' + manager_id,
                            headers: {
                                'Authorization': $localStorage.jwt
                            },
                            data: {
                                name: $scope.name,
                                level: $scope.level
                            }
                        };
                    } else {
                        var req = {
                            method: 'PATCH',
                            url: 'https://dell-api.verifi.care/managers/update' + manager_id,
                            headers: {
                                'Authorization': $localStorage.jwt
                            },
                            data: {
                                name: $scope.name,
                                level: $scope.level,
                                parent_id: $scope.parent_id
                            }
                        };
                    }

                    $http(req).then(function (data) {
                        $scope.process = false;
                        $scope.success = true;
                        $scope.waiting = false;
                        $scope.fail = false;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        }
                        $scope.fail = true;
                        $scope.process = false;
                    });
                };
            }

            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });