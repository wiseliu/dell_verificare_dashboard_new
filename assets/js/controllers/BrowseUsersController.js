angular.module("Dashboard")
        .controller("BrowseUsersController", function ($window, $location, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browseusers';
                $scope.Data.parent = 'users';
                var controller = this;
                $scope.level = $localStorage.level;
                $scope.go = function (path) {
                    $location.path($location.path() + path);
                };
                $scope.loading = true;
                var vm = this;
                vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/users/list',
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    }
                    var defer = $q.defer();
                    $http(req).then(function (data) {
                        controller.response = data.data;
                        $scope.users = controller.response.data;
                        $scope.loading = false;
                        defer.resolve($scope.users);
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {
                            $scope.users = {};
                            defer.resolve($scope.users);
                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                    return defer.promise;
                }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting',[]);
                if ($localStorage.level === 'admin') {
                    vm.dtColumns = [
                        DTColumnBuilder.newColumn('id').withTitle('ID').withOption('searchable', false),
                        DTColumnBuilder.newColumn('name').withTitle('Name'),
                        DTColumnBuilder.newColumn('username').withTitle('User Name'),
                        DTColumnBuilder.newColumn('level').withTitle('Level').withOption('searchable', false),
                        DTColumnBuilder.newColumn('manager.name').withTitle('Manager').withOption('searchable', false),
                        DTColumnBuilder.newColumn('phone').withTitle('Phone'),
                        DTColumnBuilder.newColumn('email').withTitle('Email'),
                        DTColumnBuilder.newColumn('id').withTitle('Edit').withOption('searchable', false).renderWith(function (full) {
                            return ' <a class="btn btn-success btn-small" href="/users/browse-users/' + full + '"> E d i t </a>';
                        })
                    ];
                } else {
                    vm.dtColumns = [
                        DTColumnBuilder.newColumn('id').withTitle('ID').withOption('searchable', false),
                        DTColumnBuilder.newColumn('name').withTitle('Name'),
                        DTColumnBuilder.newColumn('username').withTitle('User Name'),
                        DTColumnBuilder.newColumn('level').withTitle('Level').withOption('searchable', false),
                        DTColumnBuilder.newColumn('manager.name').withTitle('Manager').withOption('searchable', false),
                        DTColumnBuilder.newColumn('phone').withTitle('Phone'),
                        DTColumnBuilder.newColumn('email').withTitle('Email')
                    ];
                }
            }


            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });