angular.module("Dashboard")
        .controller("CreateCommentController", function ($window, $http, $scope, $localStorage, Data) {
            if ($localStorage.level !== 'admin') {
                $window.location.href = $window.location.href + '/../../dashboard';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'createcomment';
                $scope.Data.parent = 'posts';

                $scope.waiting = true;
                $scope.success = false;

                $scope.createPost = function () {
                    $scope.process = true;
                    var req = {
                        method: 'POST',
                        url: 'https://dell-api.verifi.care/posts/create_comment/',
                        headers: {
                            'Authorization': $localStorage.jwt
                        },
                        data: {
                            title: $scope.title,
                            content: $scope.content
                        }
                    };

                    $http(req).then(function (data) {
                        $scope.process = false;
                        $scope.success = true;
                        $scope.waiting = false;
                        $scope.fail = false;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        }
                        $scope.fail = true;
                        $scope.process = false;
                    });
                };

                $scope.reset = function () {
                    $scope.level = "";
                    $scope.parent_id = "";
                    $scope.name = "";

                    $scope.process = false;
                    $scope.waiting = true;
                    $scope.success = false;
                };
            }

            function injectScript() {
                $('select').select2();
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });