angular.module("Dashboard")
        .controller("SummaryController", function ($window, $scope, Data, $http, $localStorage) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'summary';
                $scope.Data.parent = 'reports';

                $scope.loading = true;

                var controller = this;

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/sellouts/recap_cam',
                    headers: {
                        'Content-Type': "application/x-www-form-urlencoded",
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    controller.response = data.data;
                    $scope.oridata = controller.response.data.value;
                    $scope.orilabels = controller.response.data.label;

                    $scope.labels = $scope.orilabels;

                    $scope.data = $scope.oridata;
                    
                    $scope.options = {
                        scales: {
                            yAxes: [{
                                    ticks: {
                                        min: 0,
                                        beginAtZero: true
                                    }
                                }]
                        }
                    }

                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/managers/list',
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    };
                    $http(req).then(function (data) {
                        controller.response = data.data;
                        $scope.managers = controller.response.data;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {

                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });

                    $scope.loading = false;
                }, function (data) {
                    if (data.status === 401) {
                        alert("Your credential is expired or invalid, please log in again");
                        $localStorage.$reset();
                        $window.location.href = 'login.html';
                    } else if (data.status === 404) {
                        $scope.mdealers = {}
                    }
                    $scope.loading = false;
                    $scope.error = true;
                });

                $scope.filterData = function () {
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/sellouts/recap_store_cam?manager_id=' + $scope.manager,
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    };

                    $http(req).then(function (data) {
                        controller.response = data.data;
                        $scope.labels = controller.response.data.label;
                        $scope.data = controller.response.data.value;

                        $scope.loading = false;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {

                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });

                };

                $scope.reset = function () {
                    $scope.labels = $scope.orilabels;
                    $scope.data = $scope.oridata;
                };
            }

            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });