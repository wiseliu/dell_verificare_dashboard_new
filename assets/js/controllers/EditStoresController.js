angular.module("Dashboard")
        .controller("EditStoresController", function ($routeParams, $window, $http, $scope, $localStorage, Data) {
            if ($localStorage.level !== 'admin') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browsestores';
                $scope.Data.parent = 'stores';
                var store_id = $routeParams.store_id;
                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/stores/list_category',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };
                $http(req).then(function (data) {
                    $scope.storecats = data.data.data.level;
                }, function (data) {

                });
                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/stores/' + store_id,
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };
                $http(req).then(function (data) {
                    $scope.store_id = data.data.data.id;
                    $scope.store_uid = data.data.data.store_uid;
                    $scope.city = data.data.data.city.id;
                    console.log($scope.city)
                    $scope.city_name = data.data.data.city.name;
                    $scope.name = data.data.data.name;
                    $scope.level = data.data.data.level;
                    $scope.address = data.data.data.address;
                    $scope.phone = data.data.data.phone;
                    $scope.email = data.data.data.email;
                    $scope.store_building = data.data.data.store_building;
                    $scope.store_category_name = data.data.data.store_category;
                    for(var storecat in $scope.storecats) {
                        if(storecat === $scope.store_category_name) {
                            $scope.store_category = $scope.storecats[storecat];
                        }
                    }
                    $scope.store_owner = data.data.data.store_owner;
                    $('select').select2();
                }, function (data) {

                });
                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/cities/list',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };
                $http(req).then(function (data) {
                    $scope.cities = data.data.data;
                }, function (data) {

                });
                
                $scope.editStore = function () {
                    $scope.process = true;

                    var req = {
                        method: 'PATCH',
                        url: 'https://dell-api.verifi.care/stores/update/' + store_id,
                        headers: {
                            'Authorization': $localStorage.jwt
                        },
                        data: {
                            city_id: $scope.city,
                            name: $scope.name,
                            level: $scope.level,
                            address: $scope.address,
                            phone: $scope.phone,
                            email: $scope.email,
                            store_building: $scope.store_building,
                            store_category: $scope.store_category,
                            store_owner: $scope.store_owner
                        }
                    };
                    $http(req).then(function (data) {
                        $scope.process = false;
                        $scope.success = true;
                        $scope.waiting = false;
                        $scope.fail = false;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        }
                        $scope.fail = true;
                        $scope.process = false;
                    });
                };
            }

            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });