angular.module("Dashboard")
        .controller("BrowseSinglePostController", function ($routeParams, $window, $location, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browseposts';
                $scope.Data.parent = 'posts';

                var controller = this;

                var post_id = $routeParams.post_id;

                $scope.loading = true;

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/posts/' + post_id,
                    headers: {
                        'Content-Type': "application/x-www-form-urlencoded",
                        'Authorization': $localStorage.jwt
                    }
                }

                $http(req).then(function (data) {
                    controller.response = data.data;

                    $scope.title = controller.response.data.title;
                    $scope.username = controller.response.data.user.username;
                    $scope.content = controller.response.data.content;
                    $scope.comments = controller.response.data.comments;
                    console.log($scope.comments);
                    $scope.loading = false;
                }, function (data) {
                    if (data.status === 401) {
                        alert("Your credential is expired or invalid, please log in again");
                        $localStorage.$reset();
                        $window.location.href = 'login.html';
                    }
                    $scope.fail = true;
                    $scope.process = false;
                });
            }

            $scope.addComment = function () {
                if (angular.isUndefined($scope.newtitle) || angular.isUndefined($scope.newcomment)) {

                } else {
                    $scope.process = true;
                    var req = {
                        method: 'POST',
                        url: 'https://dell-api.verifi.care/posts/create_comment/' + post_id,

                        headers: {
                            'Authorization': $localStorage.jwt
                        },
                        data: {
                            title: $scope.newtitle,
                            content: $scope.newcomment
                        }
                    };

                    $http(req).then(function (data) {
                        $scope.process = false;
                        $scope.success = true;
                        $scope.waiting = false;
                        $scope.fail = false;
                        $scope.refresh();

                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        }
                        $scope.fail = true;
                        $scope.process = false;
                    });
                }

            }

            $scope.refresh = function () {
                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/posts/' + post_id,
                    headers: {
                        'Content-Type': "application/x-www-form-urlencoded",
                        'Authorization': $localStorage.jwt
                    }
                }

                $http(req).then(function (data) {
                    controller.response = data.data;

                    $scope.title = controller.response.data.title;
                    $scope.username = controller.response.data.user.username;
                    $scope.content = controller.response.data.content;
                    $scope.comments = controller.response.data.comments;
                    $scope.loading = false;
                }, function (data) {
                    if (data.status === 401) {
                        alert("Your credential is expired or invalid, please log in again");
                        $localStorage.$reset();
                        $window.location.href = 'login.html';
                    }
                    $scope.fail = true;
                    $scope.process = false;
                });
            }

            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });