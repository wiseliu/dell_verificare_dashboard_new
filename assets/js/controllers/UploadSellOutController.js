angular.module("Dashboard")
        .controller("UploadSellOutController", function ($window, $scope, Data, $http, $localStorage) {
            if ($localStorage.level !== 'admin') {
                $window.location.href = $window.location.href + '/../../dashboard';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'uploadsellout';
                $scope.Data.parent = 'sellouts';

                $scope.waiting = true;
                $scope.success = false;

                $scope.submit = function () {
                    if ($scope.form.file.$valid && $scope.file) {

                        $scope.process = true;
                        $scope.fail = false;

                        var fd = new FormData();

                        fd.append("csv", $scope.file);

                        var req = {
                            method: 'POST',
                            url: 'https://dell-api.verifi.care/sellouts/import',
                            headers: {
                                'Content-Type': undefined,
                                'Authorization': $localStorage.jwt
                            },
                            data: fd,
                            transformRequest: angular.identity
                        };

                        $http(req).then(function (data) {
                            $scope.process = false;
                            $scope.waiting = false;
                            $scope.success = true;
                        }, function (data) {
                            if (data.status === 401) {
                                alert("Your credential is expired or invalid, please log in again");
                                $localStorage.$reset();
                                $window.location.href = 'login.html';
                            }
                            $scope.fail = true;

                            $scope.errmsg = "An error has occurred, please try again later";

                            $scope.waiting = true;
                            $scope.process = false;
                        });
                    } else {
                        $scope.fail = true;
                        if (!$scope.form.file.$valid) {
                            $scope.errmsg = "Only .csv files are allowed";
                        } else {
                            $scope.errmsg = "Please insert a file";
                        }
                    }

                    $scope.reset = function () {
                        $scope.file = "";

                        $scope.waiting = true;
                        $scope.process = false;
                        $scope.success = false;
                    };
                }
            }
            
            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });