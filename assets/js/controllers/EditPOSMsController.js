angular.module("Dashboard")
        .controller("EditPOSMsController", function ($routeParams, $window, $http, $scope, Data, $localStorage) {
            if ($localStorage.level !== 'admin') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browseposms';
                $scope.Data.parent = 'visibilities';

                $scope.waiting = true;
                $scope.success = false;

                var posm_id = $routeParams.posm_id;

                var controller = this;

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/posms/list_category',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.categories = data.data.data.level;
                }, function (data) {

                });

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/posms/' + posm_id,
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.id = data.data.data.id;
                    $scope.name = data.data.data.name;
                    $scope.quantity = data.data.data.quantity;
                    $scope.category = data.data.data.category;
                    for(var category in $scope.categories) {
                        if(category === $scope.category) {
                            $scope.category = $scope.categories[category];
                            alert($scope.category)
                        }
                    }
                    $scope.prevcategory_name = data.data.data.category;

                    $('select').select2();
                }, function (data) {

                });

                $scope.editPOSM = function () {
                    $scope.process = true;
                    var req = {
                        method: 'PATCH',
                        url: 'https://dell-api.verifi.care/posms/update/' + posm_id,
                        headers: {
                            'Authorization': $localStorage.jwt
                        },
                        data: {
                            name: $scope.name,
                            quantity: $scope.quantity,
                            category: $scope.category
                        }
                    };

                    $http(req).then(function (data) {
                        $scope.process = false;
                        $scope.success = true;
                        $scope.fail = false;
                        $scope.waiting = false;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        }
                        $scope.fail = true;
                        $scope.process = false;
                    });
                };
            }


            function injectScript() {

            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });