angular.module("Dashboard")
        .controller("DownloadRegionsController", function ($window, $scope, Data, $http, $localStorage) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'downloadregions';
                $scope.Data.parent = 'areas';

                $scope.waiting = true;
                $scope.success = false;

                $scope.download = function () {
                    $scope.process = true;
                    $scope.fail = false;

                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/regions/export',
                        headers: {
                            'Authorization': $localStorage.jwt
                        }
                    };

                    $http(req).then(function (data) {
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth() + 1;

                        var yyyy = today.getFullYear();
                        if (dd < 10) {
                            dd = '0' + dd;
                        }
                        if (mm < 10) {
                            mm = '0' + mm;
                        }
                        var today = dd + '_' + mm + '_' + yyyy;

                        var anchor = angular.element('<a/>');
                        anchor.css({display: 'none'}); // Make sure it's not visible
                        angular.element(document.body).append(anchor); // Attach to document

                        anchor.attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(data.data),
                            target: '_blank',
                            download: 'regions_' + today + '.csv'
                        })[0].click();

                        anchor.remove();

                        $scope.process = false;
                        $scope.waiting = false;
                        $scope.success = true;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        }
                        $scope.fail = true;

                        $scope.errmsg = "An error has occurred, please try again later";

                        $scope.waiting = true;
                        $scope.process = false;
                    });

                }
            }
            
            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });