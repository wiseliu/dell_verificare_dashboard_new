angular.module("Dashboard")
        .controller("SummaryMonthlySelloutRegionController", function ($window, $scope, Data, $localStorage, $http) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'summaryregionmonthly';
                $scope.Data.parent = 'reports';

                $scope.loading = true;

                var controller = this;

                $scope.years = [
                    2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030
                ];

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/sellouts/recap_region_monthly',
                    headers: {
                        'Content-Type': "application/x-www-form-urlencoded",
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    controller.response = data.data;
                    $scope.oridata = controller.response.data.value;
                    $scope.orilabels = controller.response.data.label;
                    $scope.oritabledata = {data: []};
                    for (var key in $scope.oridata) {
                        $scope.oritabledata.data.push(
                                {
                                    label: $scope.orilabels[key],
                                    value: $scope.oridata[key]
                                }
                        )
                    }

                    $scope.labels = $scope.orilabels;
                    $scope.data = $scope.oridata;
                    $scope.tabledata = $scope.oritabledata;

                    if ($scope.data.length === 0) {
                        $scope.dataIsEmpty = true;
                    }

                    $scope.options = {
                        scales: {
                            yAxes: [{
                                    ticks: {
                                        min: 0,
                                        beginAtZero: true
                                    }
                                }]
                        }
                    }

                    $scope.loading = false;
                }, function (data) {
                    if (data.status === 401) {
                        alert("Your credential is expired or invalid, please log in again");
                        $localStorage.$reset();
                        $window.location.href = 'login.html';
                    } else if (data.status === 404) {
                    }
                    $scope.loading = false;
                    $scope.error = true;
                });

                $scope.retry = function () {
                    $scope.process = true;
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/sellouts/recap_region_monthly',
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    };

                    $http(req).then(function (data) {
                        controller.response = data.data;
                        $scope.oridata = controller.response.data.value;
                        $scope.orilabels = controller.response.data.label;
                        $scope.oritabledata = {data: []};
                        for (var key in $scope.oridata) {
                            $scope.oritabledata.data.push(
                                    {
                                        label: $scope.orilabels[key],
                                        value: $scope.oridata[key]
                                    }
                            )
                        }

                        $scope.labels = $scope.orilabels;
                        $scope.data = $scope.oridata;
                        $scope.tabledata = $scope.oritabledata;
                        if ($scope.data.length > 0) {
                            $scope.dataIsEmpty = false;
                        }

                        $scope.options = {
                            scales: {
                                yAxes: [{
                                        ticks: {
                                            min: 0,
                                            beginAtZero: true
                                        }
                                    }]
                            }
                        }

                        $scope.process = false;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {
                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                }

                $scope.filterData = function () {
                    if (angular.isUndefined($scope.year)) {
                        var req = {
                            method: 'GET',
                            url: 'https://dell-api.verifi.care/sellouts/recap_region_monthly?month=' + $scope.month,
                            headers: {
                                'Content-Type': "application/x-www-form-urlencoded",
                                'Authorization': $localStorage.jwt
                            }
                        };
                    } else {
                        var req = {
                            method: 'GET',
                            url: 'https://dell-api.verifi.care/sellouts/recap_region_monthly?month=' + $scope.month
                                    + '&year=' + $scope.year
                            ,
                            headers: {
                                'Authorization': $localStorage.jwt
                            }
                        };
                    }


                    $http(req).then(function (data) {
                        controller.response = data.data;
                        $scope.labels = controller.response.data.label;
                        $scope.data = controller.response.data.value;
                        $scope.tabledata = {data: []};
                        for (var key in $scope.data) {
                            $scope.tabledata.data.push(
                                    {
                                        label: $scope.labels[key],
                                        value: $scope.data[key]
                                    }
                            )
                        }

                        $scope.loading = false;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {

                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });

                };

                $scope.reset = function () {
                    $scope.labels = $scope.orilabels;
                    $scope.data = $scope.oridata;
                    $scope.tabledata = $scope.oritabledata;
                };
            }

            function injectScript() {

            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });