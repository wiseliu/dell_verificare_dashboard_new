angular.module("Dashboard")
        .controller("BrowsePostsController", function ($filter, $window, $location, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browseposts';
                $scope.Data.parent = 'posts';
                var controller = this;
                $scope.loading = true;
                var vm = this;
                vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/posts/list',
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    }
                    var defer = $q.defer();
                    $http(req).then(function (data) {
                        controller.response = data.data;
                        $scope.posts = controller.response.data;
                        $scope.loading = false;
                        defer.resolve($scope.posts);
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {
                            $scope.posts = {};
                            defer.resolve($scope.posts);
                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                    return defer.promise;
                }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting',[]);
                vm.dtColumns = [
                    DTColumnBuilder.newColumn('id').withTitle('ID'),
                    DTColumnBuilder.newColumn('title').withTitle('Title'),
                    DTColumnBuilder.newColumn('content').withTitle('Content').renderWith(function (full) {
                        if (full.length > 45) {
                            return full.substring(0, 45) + '...';
                        } else {
                            return full;
                        }

                    }),
                    DTColumnBuilder.newColumn('user.username').withTitle('Created By'),
                    DTColumnBuilder.newColumn('level').withTitle('Level'),
                    DTColumnBuilder.newColumn('comments.length').withTitle('Comments'),
                    DTColumnBuilder.newColumn('id').withTitle('View').renderWith(function (full) {
                        return ' <a class="btn btn-success btn-small" href="/posts/browse-posts/' + full + '"> View </a>';
                    })
                ];
            }
            
            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });