angular.module("Dashboard")
        .controller("CreateSellOutController", function ($window, $http, $scope, $localStorage, Data) {
            if ($localStorage.level !== 'admin') {
                $window.location.href = $window.location.href + '/../../dashboard';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'createsellout';
                $scope.Data.parent = 'sellouts';

                $scope.waiting = true;
                $scope.success = false;



                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/stores/list',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.stores = data.data.data;
                }, function (data) {

                });

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/users/list',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.users = data.data.data;
                }, function (data) {

                });

                $scope.createSellout = function () {
                    if ($scope.form.file.$valid && $scope.file) {
                        $scope.process = true;
                        var fd = new FormData();

                        fd.append("service_tag", $scope.servtag);
                        fd.append("proof", $scope.file);
                        fd.append("store_id", $scope.store_id);
                        fd.append("added_by", $scope.added_by);
                        fd.append("sells_date", $scope.sells_date);

                        var req = {
                            method: 'POST',
                            url: 'https://dell-api.verifi.care/sellouts/create',
                            headers: {
                                'Content-Type': undefined,
                                'Authorization': $localStorage.jwt
                            },
                            data: fd,
                            transformRequest: angular.identity
                        };

                        $http(req).then(function (data) {
                            $scope.process = false;
                            $scope.success = true;
                            $scope.waiting = false;
                            $scope.fail = false;
                        }, function (data) {
                            if (data.status === 401) {
                                alert("Your credential is expired or invalid, please log in again");
                                $localStorage.$reset();
                                $window.location.href = 'login.html';
                            }
                            $scope.fail = true;
                            $scope.process = false;
                            $scope.errmsg = "An error has occurred, please try again later";
                        });
                    } else {
                        $scope.fail = true;
                        if (!$scope.form.file.$valid) {
                            $scope.errmsg = "Only jpeg, jpg, gif, and png files under 1MB are allowed";
                        } else {
                            $scope.errmsg = "Please insert a file";
                        }
                    }
                };

                $scope.reset = function () {
                    $scope.level = "";
                    $scope.parent_id = "";
                    $scope.name = "";

                    $scope.process = false;
                    $scope.waiting = true;
                    $scope.success = false;
                };
            }

            function injectScript() {
                $('select').select2();
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });