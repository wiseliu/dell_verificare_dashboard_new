angular.module("Dashboard")
        .controller("SummarySelloutCamController", function ($window, $scope, Data, $localStorage, $http, DTOptionsBuilder, DTColumnBuilder, $q) {
            $scope.Data = Data;
            $scope.Data.link = 'summarycam';
            $scope.Data.parent = 'reports';

            $scope.loading = true;
            $scope.quarter_years = [
                2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030
            ];
            var controller = this;

            var req = {
                method: 'GET',
                url: 'https://dell-api.verifi.care/sellouts/recap_cam',
                headers: {
                    'Content-Type': "application/x-www-form-urlencoded",
                    'Authorization': $localStorage.jwt
                }
            };

            $http(req).then(function (data) {
                controller.response = data.data;
                $scope.oridata = controller.response.data.value;
                $scope.orilabels = controller.response.data.label;
                $scope.oritabledata = {data: []};
                for (var key in $scope.oridata) {
                    $scope.oritabledata.data.push(
                            {
                                label: $scope.orilabels[key],
                                value: $scope.oridata[key]
                            }
                    )
                }
//                console.log($scope.oritabledata);
                $scope.options = {
                    title: {
                        display: true,
                        text: 'Sellout by Managers'
                    }
                }

                $scope.labels = $scope.orilabels;
                $scope.data = $scope.oridata;
                $scope.tabledata = $scope.oritabledata;

                $scope.data = $scope.oridata;
                if ($scope.data.length === 0) {
                    $scope.dataIsEmpty = true;
                }

                $scope.options = {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    min: 0,
                                    beginAtZero: true
                                }
                            }]
                    }
                }

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/managers/list',
                    headers: {
                        'Content-Type': "application/x-www-form-urlencoded",
                        'Authorization': $localStorage.jwt
                    }
                };
                $http(req).then(function (data) {
                    controller.response = data.data;
                    $scope.managers = controller.response.data;

                    $('select').select2();
                }, function (data) {
                    if (data.status === 401) {
                        alert("Your credential is expired or invalid, please log in again");
                        $localStorage.$reset();
                        $window.location.href = 'login.html';
                    } else if (data.status === 404) {

                    }
                    $scope.loading = false;
                    $scope.error = true;
                });

                $scope.loading = false;
            }, function (data) {
                if (data.status === 401) {
                    alert("Your credential is expired or invalid, please log in again");
                    $localStorage.$reset();
                    $window.location.href = 'login.html';
                } else if (data.status === 404) {
                }
                $scope.loading = false;
                $scope.error = true;
            });

            $scope.retry = function () {
                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/sellouts/recap_cam',
                    headers: {
                        'Content-Type': "application/x-www-form-urlencoded",
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    controller.response = data.data;
                    $scope.oridata = controller.response.data.value;
                    $scope.orilabels = controller.response.data.label;
                    $scope.oritabledata = {data: []};
                    for (var key in $scope.oridata) {
                        $scope.oritabledata.data.push(
                                {
                                    label: $scope.orilabels[key],
                                    value: $scope.oridata[key]
                                }
                        )
                    }

                    $scope.options = {
                        title: {
                            display: true,
                            text: 'Sellout by Managers'
                        }
                    }

                    $scope.labels = $scope.orilabels;
                    $scope.data = $scope.oridata;
                    $scope.tabledata = $scope.oritabledata;

                    $scope.data = $scope.oridata;
                    if ($scope.data.length > 0) {
                        $scope.dataIsEmpty = false;
                    }

                    $scope.options = {
                        scales: {
                            yAxes: [{
                                    ticks: {
                                        min: 0,
                                        beginAtZero: true
                                    }
                                }]
                        }
                    }

                    $scope.process = false;
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/managers/list',
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    };
                    $http(req).then(function (data) {
                        controller.response = data.data;
                        $scope.managers = controller.response.data;

                        $('select').select2();
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {

                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });

                    $scope.loading = false;
                }, function (data) {
                    if (data.status === 401) {
                        alert("Your credential is expired or invalid, please log in again");
                        $localStorage.$reset();
                        $window.location.href = 'login.html';
                    } else if (data.status === 404) {
                    }
                    $scope.loading = false;
                    $scope.error = true;
                });
            }

            $scope.filterData = function () {
                if (angular.isUndefined($scope.quarter_year_from)
                        || angular.isUndefined($scope.quarter_from)
                        || angular.isUndefined($scope.quarter_week_from)
                        || $scope.quarter_year_from === null
                        || $scope.quarter_from === null
                        || $scope.quarter_week_from === null
                        || angular.isUndefined($scope.quarter_year_to)
                        || angular.isUndefined($scope.quarter_to)
                        || angular.isUndefined($scope.quarter_week_to)
                        || $scope.quarter_year_to === null
                        || $scope.quarter_to === null
                        || $scope.quarter_week_to === null
                        ) {
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/sellouts/recap_store_cam?manager_id=' + $scope.manager,
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    };
                } else {
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/sellouts/recap_store_cam?manager_id=' + $scope.manager
                                + '&quarter_year_from=' + $scope.quarter_year_from
                                + '&quarter_from=' + $scope.quarter_from
                                + '&quarter_week_from=' + $scope.quarter_week_from
                                + '&quarter_year_to=' + $scope.quarter_year_to
                                + '&quarter_to=' + $scope.quarter_to
                                + '&quarter_week_to=' + $scope.quarter_week_to
                        ,
                        headers: {
                            'Authorization': $localStorage.jwt
                        }
                    };
                }

                $http(req).then(function (data) {
                    console.log(data);
                    controller.response = data.data;
                    $scope.labels = controller.response.data.label;
                    $scope.data = controller.response.data.value;
                    $scope.tabledata = {data: []};
                    for (var key in $scope.data) {
                        $scope.tabledata.data.push(
                                {
                                    label: $scope.labels[key],
                                    value: $scope.data[key]
                                }
                        )
                    }

                    $scope.loading = false;
                }, function (data) {
                    if (data.status === 401) {
                        alert("Your credential is expired or invalid, please log in again");
                        $localStorage.$reset();
                        $window.location.href = 'login.html';
                    } else if (data.status === 404) {

                    }
                    $scope.loading = false;
                    $scope.error = true;
                });

            };

            $scope.filterQuarter = function () {
                if (angular.isUndefined($scope.quarter_year_from)
                        || angular.isUndefined($scope.quarter_from)
                        || angular.isUndefined($scope.quarter_week_from)
                        || $scope.quarter_year_from === null
                        || $scope.quarter_from === null
                        || $scope.quarter_week_from === null
                        || angular.isUndefined($scope.quarter_year_to)
                        || angular.isUndefined($scope.quarter_to)
                        || angular.isUndefined($scope.quarter_week_to)
                        || $scope.quarter_year_to === null
                        || $scope.quarter_to === null
                        || $scope.quarter_week_to === null
                        ) {
                    $scope.fail = true;
                } else {
                    $scope.fail = false;
                    if (angular.isUndefined($scope.manager)) {
                        var req = {
                            method: 'GET',
                            url: 'https://dell-api.verifi.care/sellouts/recap_cam?'
                                    + 'quarter_year_from=' + $scope.quarter_year_from
                                    + '&quarter_from=' + $scope.quarter_from
                                    + '&quarter_week_from=' + $scope.quarter_week_from
                                    + '&quarter_year_to=' + $scope.quarter_year_to
                                    + '&quarter_to=' + $scope.quarter_to
                                    + '&quarter_week_to=' + $scope.quarter_week_to
                            ,
                            headers: {
                                'Authorization': $localStorage.jwt
                            }
                        };
                    } else {
                        var req = {
                            method: 'GET',
                            url: 'https://dell-api.verifi.care/sellouts/recap_store_cam?manager_id=' + $scope.manager
                                    + '&quarter_year_from=' + $scope.quarter_year_from
                                    + '&quarter_from=' + $scope.quarter_from
                                    + '&quarter_week_from=' + $scope.quarter_week_from
                                    + '&quarter_year_to=' + $scope.quarter_year_to
                                    + '&quarter_to=' + $scope.quarter_to
                                    + '&quarter_week_to=' + $scope.quarter_week_to
                            ,
                            headers: {
                                'Authorization': $localStorage.jwt
                            }
                        };
                    }

                    $http(req).then(function (data) {
                        controller.response = data.data;
                        $scope.data = controller.response.data.value;
                        $scope.labels = controller.response.data.label;
                        $scope.tabledata = {data: []};
                        for (var key in $scope.data) {
                            $scope.tabledata.data.push(
                                    {
                                        label: $scope.labels[key],
                                        value: $scope.data[key]
                                    }
                            )
                        }
                        $scope.options = {
                            title: {
                                display: true,
                                text: 'Sellout by Managers'
                            }
                        }

                        $scope.loading = false;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {
                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                }
            }

            $scope.reset = function () {
                $scope.labels = $scope.orilabels;
                $scope.data = $scope.oridata;
                $scope.tabledata = $scope.oritabledata;
            };

            function injectScript() {
                $('select').select2();
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });