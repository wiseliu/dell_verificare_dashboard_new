angular.module("Dashboard")
        .controller("SearchSellInController", function ($routeParams, $window, $location, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'searchsellin';
                $scope.Data.parent = 'sellins';
                $scope.filtered = false;
                $scope.loading = true;

                $scope.service_tag_filter = $routeParams.filter;

                var controller = this;
                if (angular.isUndefined($scope.service_tag_filter)) {
                    $scope.filtered = false;
                } else {
                    if ($scope.service_tag_filter.indexOf(',') > -1) {
                        url = 'https://dell-api.verifi.care/sellins/bulk_search?q=' + $scope.service_tag_filter;
                    } else {
                        url = 'https://dell-api.verifi.care/sellins/search?q=' + $scope.service_tag_filter;
                    }
                    $scope.filtered = true;
                    var vm = this;
                    vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                        var req = {
                            method: 'GET',
                            url: url,
                            headers: {
                                'Content-Type': "application/x-www-form-urlencoded",
                                'Authorization': $localStorage.jwt
                            }
                        }
                        $scope.defer = $q.defer();
                        $http(req).then(function (data) {
                            controller.response = data.data;

                            $scope.sellins = controller.response.data;
                            $scope.loading = false;
                            $scope.defer.resolve($scope.sellins);
                        }, function (data) {
                            if (data.status === 401) {
                                alert("Your credential is expired or invalid, please log in again");
                                $localStorage.$reset();
                                $window.location.href = 'login.html';
                            } else if (data.status === 404) {
                                $scope.sellins='';
                                $scope.defer.resolve($scope.sellins);
                            }
                            $scope.loading = false;
                            $scope.error = true;
                        });
                        return $scope.defer.promise;
                    }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting',[]);

                    vm.dtColumns = [
                        DTColumnBuilder.newColumn('id').withTitle('ID'),
                        DTColumnBuilder.newColumn('service_tag').withTitle('Service Tag'),
                        DTColumnBuilder.newColumn('quarter_year').withTitle('Quarter Year'),
                        DTColumnBuilder.newColumn('quarter').withTitle('Quarter'),
                        DTColumnBuilder.newColumn('quarter_week').withTitle('Quarter Week'),
                        DTColumnBuilder.newColumn('item_type').withTitle('Item Type'),
                        DTColumnBuilder.newColumn('part_number').withTitle('Part Number'),
                        DTColumnBuilder.newColumn('product_type').withTitle('Product Type'),
                        DTColumnBuilder.newColumn('product_name').withTitle('Product Name'),
                        DTColumnBuilder.newColumn('source_store').withTitle('Source Store'),
                        DTColumnBuilder.newColumn('target_store').withTitle('Target Store')
                    ];
                }

                $scope.filterSearch = function () {
                    if (!angular.isUndefined($scope.service_tag_filter)) {
                        $location.path('sellins/search-sell-in/' + $scope.service_tag_filter);
                    }
                }
            }

            function injectScript() {

            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });
