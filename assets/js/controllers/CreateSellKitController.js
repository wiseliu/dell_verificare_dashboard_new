angular.module("Dashboard")
        .controller("CreateSellKitController", function ($window, $scope, Data, $http, $localStorage) {
            if ($localStorage.level !== 'admin') {
                $window.location.href = $window.location.href + '/../../dashboard';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'createsellkit';
                $scope.Data.parent = 'sellkits';

                $scope.waiting = true;
                $scope.success = false;

                var fam = {
                    "families": [
                        {"id": 1, "name": "Inspiron 3000"},
                        {"id": 2, "name": "Inspiron 5000"},
                        {"id": 3, "name": "Inspiron 7000"},
                        {"id": 4, "name": "Vostro"},
                        {"id": 5, "name": "XPS 12"},
                        {"id": 6, "name": "XPS 13"},
                        {"id": 7, "name": "XPS 15"},
                        {"id": 8, "name": "Inspiron Desktop"},
                        {"id": 9, "name": "Inspiron Aio"},
                        {"id": 10, "name": "Monitor"},
                        {"id": 11, "name": "Promotion"},
                    ]
                };
                
                $scope.families = fam.families;

                

                $scope.createSellKit = function () {
                    if ($scope.form.file.$valid && $scope.file) {

                        $scope.process = true;
                        $scope.fail = false;

                        var fd = new FormData();

                        fd.append("name", $scope.name);
                        fd.append("description", $scope.description);
                        fd.append("category", $scope.category);
                        fd.append("family", $scope.family);
                        fd.append("file_name", $scope.file);

                        var req = {
                            method: 'POST',
                            url: 'https://dell-api.verifi.care/sell_kits/create',
                            headers: {
                                'Content-Type': undefined,
                                'Authorization': $localStorage.jwt
                            },
                            data: fd,
                            transformRequest: angular.identity
                        };

                        $http(req).then(function (data) {
                            $scope.process = false;
                            $scope.waiting = false;
                            $scope.success = true;
                        }, function (data) {
                            if (data.status === 401) {
                                alert("Your credential is expired or invalid, please log in again");
                                $localStorage.$reset();
                                $window.location.href = 'login.html';
                            }
                            $scope.fail = true;

                            $scope.errmsg = "An error has occurred, please try again later";

                            $scope.waiting = true;
                            $scope.process = false;
                        });
                    } else {
                        $scope.fail = true;
                        if (!$scope.form.file.$valid) {
                            $scope.errmsg = "Only .doc or .pdf files are allowed";
                        } else {
                            $scope.errmsg = "Please insert a file";
                        }
                    }

                    $scope.reset = function () {
                        $scope.file = "";

                        $scope.waiting = true;
                        $scope.process = false;
                        $scope.success = false;
                    };
                }
            }

            function injectScript() {
                $('select').select2();
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });