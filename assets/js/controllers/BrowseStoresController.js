angular.module("Dashboard")
        .controller("BrowseStoresController", function ($window, $location, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browsestores';
                $scope.Data.parent = 'stores';

                var controller = this;
                $scope.level = $localStorage.level;

                $scope.go = function (path) {
                    $location.path($location.path() + path);
                };

                var vm = this;
                vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    var defer = $q.defer();
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/stores/list',
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    }

                    $http(req).then(function (data) {
                        controller.response = data.data;

                        $scope.stores = controller.response.data;
                        $scope.loading = false;
                        defer.resolve($scope.stores);

                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {
                            $scope.stores = {};
                            defer.resolve($scope.stores);
                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                    return defer.promise;
                }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting',[]);

                if ($localStorage.level === 'admin') {
                    vm.dtColumns = [
                        DTColumnBuilder.newColumn('id').withTitle('ID'),
                        DTColumnBuilder.newColumn('name').withTitle('Name'),
                        DTColumnBuilder.newColumn('store_uid').withTitle('Store Unique ID'),
                        DTColumnBuilder.newColumn('address').withTitle('Address'),
                        DTColumnBuilder.newColumn('phone').withTitle('Phone'),
                        DTColumnBuilder.newColumn('email').withTitle('Email'),
                        DTColumnBuilder.newColumn('store_building').withTitle('Building Name'),
                        DTColumnBuilder.newColumn('city.name').withTitle('City'),
                        DTColumnBuilder.newColumn('id').withTitle('Edit').renderWith(function (full) {
                            return ' <a class="btn btn-success btn-small" href="/stores/browse-stores/' + full + '"> Edit </a>';
                        })
                    ];
                } else {
                    vm.dtColumns = [
                        DTColumnBuilder.newColumn('id').withTitle('ID'),
                        DTColumnBuilder.newColumn('name').withTitle('Name'),
                        DTColumnBuilder.newColumn('store_uid').withTitle('Store Unique ID'),
                        DTColumnBuilder.newColumn('address').withTitle('Address'),
                        DTColumnBuilder.newColumn('phone').withTitle('Phone'),
                        DTColumnBuilder.newColumn('email').withTitle('Email'),
                        DTColumnBuilder.newColumn('store_building').withTitle('Building Name'),
                        DTColumnBuilder.newColumn('city.name').withTitle('City')
                    ];
                }
            }
            
            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });