angular.module("Dashboard")
        .controller("EditSellOutController", function ($routeParams, $window, $http, $scope, $localStorage, Data) {
            if ($localStorage.level !== 'admin') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browsesellouts';
                $scope.Data.parent = 'sellouts';

                $scope.waiting = true;
                $scope.success = false;

                var sellout_id = $routeParams.sellout_id;

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/sellouts/' + sellout_id,
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.service_tag = data.data.data.service_tag;
                    $scope.store_id = data.data.data.store.id;
                    $scope.added_by = data.data.data.user.id;
                    $scope.url = data.data.data.proof.proof.url;

                }, function (data) {

                });

                $scope.editSellout = function () {
                    if ($scope.form.file.$valid && $scope.file) {
                        $scope.process = true;
                        var fd = new FormData();

                        fd.append("service_tag", $scope.service_tag);
                        fd.append("proof", $scope.file);
                        fd.append("store_id", $scope.store_id);
                        fd.append("added_by", $scope.added_by);

                        var req = {
                            method: 'PATCH',
                            url: 'https://dell-api.verifi.care/sellouts/update/' + sellout_id,
                            headers: {
                                'Content-Type': undefined,
                                'Authorization': $localStorage.jwt
                            },
                            data: fd,
                            transformRequest: angular.identity
                        };

                        $http(req).then(function (data) {
                            $scope.process = false;
                            $scope.success = true;
                            $scope.waiting = false;
                            $scope.fail = false;
                        }, function (data) {
                            if (data.status === 401) {
                                alert("Your credential is expired or invalid, please log in again");
                                $localStorage.$reset();
                                $window.location.href = 'login.html';
                            }
                            $scope.fail = true;
                            $scope.process = false;
                        });
                    } else {
                        $scope.fail = true;
                        if (!$scope.form.file.$valid) {
                            $scope.errmsg = "Only jpeg, jpg, gif, and png files under 1MB are allowed";
                        } else {
                            $scope.errmsg = "Please insert a file";
                        }
                    }
                };
            }

            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });