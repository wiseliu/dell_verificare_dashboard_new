angular.module("Dashboard")
        .controller("BrowseMasterDealersController", function ($window, $location, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browsemasterdealers';
                $scope.Data.parent = 'stores';

                var controller = this;

                var vm = this;
                vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    var defer = $q.defer();
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/dealers/list',
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    }

                    $http(req).then(function (data) {
                        controller.response = data.data;

                        $scope.mdealers = controller.response.data;
                        $scope.loading = false;
                        defer.resolve($scope.mdealers);

                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {
                            $scope.mdealers = {}
                            defer.resolve($scope.mdealers);
                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                    return defer.promise;
                }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting',[]);

                vm.dtColumns = [
                    DTColumnBuilder.newColumn('id').withTitle('ID'),
                    DTColumnBuilder.newColumn('name').withTitle('Name'),
                    DTColumnBuilder.newColumn('store_uid').withTitle('Store Unique ID'),
                    DTColumnBuilder.newColumn('address').withTitle('Address'),
                    DTColumnBuilder.newColumn('phone').withTitle('Phone'),
                    DTColumnBuilder.newColumn('email').withTitle('Email'),
                    DTColumnBuilder.newColumn('store_building').withTitle('Building Name'),
                    DTColumnBuilder.newColumn('city.name').withTitle('City')
                ];
            }

            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });