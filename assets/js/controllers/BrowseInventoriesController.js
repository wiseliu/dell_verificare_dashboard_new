angular.module("Dashboard")
        .controller("BrowseInventoriesController", function ($filter, $window, $location, $routeParams, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browseinventories';
                $scope.Data.parent = 'stocks';

                $scope.loading = true;

                var controller = this;
                
                $scope.page = $routeParams.page;
                if (angular.isUndefined($scope.page)) {
                    $scope.page = 1;
                }
                $scope.pageSize = 1000;

                var vm = this;
                vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/inventories/list?p='+$scope.page,
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    }
                    $scope.defer = $q.defer();
                    $http(req).then(function (data) {
                        controller.response = data.data;
                        
                        $scope.currentPage = $scope.page;
                        $scope.total = controller.response.totalData;
                        $scope.inventories = controller.response.data;
                        $scope.loading = false;
                        $scope.defer.resolve($scope.inventories);
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {
                            $scope.inventories = '';
                            $scope.loading = false;
                            $scope.defer.resolve($scope.inventories);
                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                    return $scope.defer.promise;
                }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting',[]);

                vm.dtColumns = [
                    DTColumnBuilder.newColumn('id').withTitle('ID'),
                    DTColumnBuilder.newColumn('service_tag').withTitle('service_tag'),
                    DTColumnBuilder.newColumn('updated_at').withTitle('Updated At').renderWith(function (data, type, full) {
                        return $filter('date')(data, "dd/MM/yyyy HH:mm");
                    }),
                    DTColumnBuilder.newColumn('store.store_uid').withTitle('Store Unique ID'),
                    DTColumnBuilder.newColumn('store.name').withTitle('Store Name'),
                    DTColumnBuilder.newColumn('user.name').withTitle('Added By')
                ];
            }

            $scope.changepage = function (page) {
                $location.path('stocks/browse-inventories/page/' + page);
            };
            
            function injectScript() {

            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });
