angular.module("Dashboard")
        .controller("AddAreaController", function ($window, $http, $scope, $localStorage, Data) {
            if ($localStorage.level !== 'admin') {
                $window.location.href = $window.location.href + '/../../dashboard';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'addarea';
                $scope.Data.parent = 'areas';

                $scope.waiting = true;
                $scope.success = false;



                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/regions/list',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.regions = data.data.data;
                }, function (data) {

                });

                $scope.addArea = function () {
                    $scope.process = true;
                    var req = {
                        method: 'POST',
                        url: 'https://dell-api.verifi.care/cities/create',
                        headers: {
                            'Authorization': $localStorage.jwt
                        },
                        data: {
                            name: $scope.cityname,
                            region_id: $scope.region
                        }
                    };

                    $http(req).then(function (data) {
                        $scope.process = false;
                        $scope.success = true;
                        $scope.waiting = false;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        }
                        $scope.fail = true;
                    });
                };

                $scope.reset = function () {
                    $scope.cityname = "";

                    $scope.process = false;
                    $scope.waiting = true;
                    $scope.success = false;
                };

            }

            function injectScript() {
                $('select').select2();
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });