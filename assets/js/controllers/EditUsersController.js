angular.module("Dashboard")
        .controller("EditUsersController", function ($routeParams, $window, $http, $scope, Data, $localStorage) {
            if ($localStorage.level !== 'admin') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browseusers';
                $scope.Data.parent = 'users';

                $scope.waiting = true;
                $scope.success = false;

                var username = $routeParams.username;

                var controller = this;

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/managers/list',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.managers = data.data.data;
                }, function (data) {

                });
                
                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/users/list_level',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.levels = data.data.data.level;
                    delete $scope.levels.admin;
                }, function (data) {

                });

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/users/' + username,
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.id = data.data.data.id;
                    $scope.username = data.data.data.username;
                    $scope.password_digest = data.data.data.password;
                    $scope.level = data.data.data.level;
                    for(var level in $scope.levels) {
                        if(level === $scope.level) {
                            $scope.level = $scope.levels[level];
                        }
                    }
                    $scope.prevlevel = data.data.data.level;
                    $scope.manager = data.data.data.manager;
                    for(var manager in $scope.managers) {
                        if($scope.managers[manager].name === $scope.manager.name) {
                            $scope.manager = $scope.managers[manager].id;
                            console.log($scope.manager);
                        }
                    }
                    $scope.prevmanager = data.data.data.manager;
                    $scope.name = data.data.data.name;
                    $scope.email = data.data.data.email;
                    $scope.phone = data.data.data.phone;

                    $('select').select2();
                }, function (data) {

                });

                $scope.editUser = function () {
                    $scope.process = true;
                    if (angular.isUndefined($scope.parent_id)) {
                        var req = {
                            method: 'PATCH',
                            url: 'https://dell-api.verifi.care/users/update/' + username,
                            headers: {
                                'Authorization': $localStorage.jwt
                            },
                            data: {
                                level: $scope.level,
                                manager_id: $scope.manager,
                                name: $scope.name,
                                email: $scope.email,
                                phone: $scope.phone
                            }
                        };
                    } else {
                        var req = {
                            method: 'PATCH',
                            url: 'https://dell-api.verifi.care/users/update/' + username,
                            headers: {
                                'Authorization': $localStorage.jwt
                            },
                            data: {
                                password_digest: $scope.password,
                                level: $scope.level,
                                manager_id: $scope.manager,
                                name: $scope.name,
                                email: $scope.email,
                                phone: $scope.phone
                            }
                        };
                    }

                    $http(req).then(function (data) {
                        $scope.process = false;
                        $scope.success = true;
                        $scope.fail = false;
                        $scope.waiting = false;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        }
                        $scope.fail = true;
                        $scope.process = false;
                    });
                };
            }


            function injectScript() {

            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });