angular.module("Dashboard")
        .controller("AddStoreController", function ($window, $http, $scope, $localStorage, Data) {
            if ($localStorage.level !== 'admin') {
                $window.location.href = $window.location.href + '/../../dashboard';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'addstore';
                $scope.Data.parent = 'stores';

                $scope.waiting = true;
                $scope.success = false;

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/stores/list_level',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.levels = data.data.data.level;
                }, function (data) {

                });

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/stores/list_category',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.storecats = data.data.data.level;
                }, function (data) {

                });

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/cities/list',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.cities = data.data.data;
                }, function (data) {

                });

                $scope.addStore = function () {
                    $scope.process = true;
                    var req = {
                        method: 'POST',
                        url: 'https://dell-api.verifi.care/stores/create',
                        headers: {
                            'Authorization': $localStorage.jwt
                        },
                        data: {
                            store_uid: $scope.store_uid,
                            city_id: $scope.city,
                            name: $scope.name,
                            level: $scope.level,
                            address: $scope.address,
                            phone: $scope.phone,
                            email: $scope.email,
                            store_building: $scope.store_building,
                            store_category: $scope.store_category,
                            store_owner: $scope.store_owner
                        }
                    };

                    $http(req).then(function (data) {
                        $scope.process = false;
                        $scope.success = true;
                        $scope.waiting = false;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        }
                        $scope.process = false;
                        $scope.fail = true;
                    });
                };

                $scope.reset = function () {
                    $scope.city = "";
                    $scope.name = "";
                    $scope.level = "";
                    $scope.address = "";
                    $scope.phone = "";
                    $scope.email = "";
                    $scope.store_building = "";
                    $scope.store_category = "";
                    $scope.store_owner = "";

                    $scope.process = false;
                    $scope.waiting = true;
                    $scope.success = false;
                };
            }
            
            function injectScript() {
                $('select').select2();
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });

        