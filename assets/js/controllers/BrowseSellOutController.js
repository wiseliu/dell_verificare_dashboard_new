angular.module("Dashboard")
        .controller("BrowseSellOutController", function ($filter, $window, $location, $routeParams, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browsesellouts';
                $scope.Data.parent = 'sellouts';

                $scope.loading = true;

                var controller = this;

                $scope.page = $routeParams.page;
                if (angular.isUndefined($scope.page)) {
                    $scope.page = 1;
                }

                var controller = this;
                $scope.pageSize = 1000;

                var vm = this;
                vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/sellouts/list?p=' + $scope.page,
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    }
                    $scope.defer = $q.defer();
                    $http(req).then(function (data) {
                        controller.response = data.data;

                        $scope.currentPage = $scope.page;
                        $scope.total = controller.response.totalData;
                        $scope.sellouts = controller.response.data;
                        $scope.loading = false;
                        $scope.defer.resolve($scope.sellouts);
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {
                            $scope.sellouts = '';
                            $scope.loading = false;
                            $scope.defer.resolve($scope.sellouts);
                        } else {

                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                    return $scope.defer.promise;
                }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting',[]);

                vm.dtColumns = [
                    DTColumnBuilder.newColumn('id').withTitle('ID'),
                    DTColumnBuilder.newColumn('service_tag').withTitle('Service Tag'),
                    DTColumnBuilder.newColumn('updated_at').withTitle('Updated At').renderWith(function (data, type, full) {
                        return $filter('date')(data, "dd/MM/yyyy HH:mm");
                    }),
                    DTColumnBuilder.newColumn('store.store_uid').withTitle('Store Uid'),
                    DTColumnBuilder.newColumn('store.name').withTitle('Store Name'),
                    DTColumnBuilder.newColumn('user.username').withTitle('Username'),
                    DTColumnBuilder.newColumn('user.name').withTitle('User Name'),
                    DTColumnBuilder.newColumn('proof.proof.url').withTitle('Proof').renderWith(function (data, type, full) {
                        return "<a href='" + full.proof.proof.url + "'><img class='tableimg' src='" + full.proof.proof.url + "' alt='Proof image'></img></a>";
                    }),
                    DTColumnBuilder.newColumn('id').withTitle('Edit').renderWith(function (full) {
                        return ' <a class="btn btn-success btn-small" href="/sellouts/browse-sell-out/' + full + '"> Edit Proof </a>';
                    })
                ];
            }

            $scope.changepage = function (page) {
                $location.path('sellouts/browse-sell-out/page/' + page);
            };
            
            function injectScript() {

            };
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });
