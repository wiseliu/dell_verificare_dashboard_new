angular.module("Dashboard")
        .controller("AbsenceSelfController", function ($window, $location, $filter, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level === 'admin' || $localStorage.level === 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'absence';
                $scope.Data.parent = 'reports';

                var controller = this;

                $scope.go = function (path) {
                    $location.path($location.path() + path);
                };

                var vm = this;
                vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    var defer = $q.defer();
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/absences/list_self',
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    }

                    $http(req).then(function (data) {
                        controller.response = data.data;

                        $scope.absences = controller.response.data;
                        $scope.loading = false;
                        defer.resolve($scope.absences);

                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {
                            $scope.absences = {}
                            defer.resolve($scope.stores);
                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                    return defer.promise;
                }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting',[]);

                vm.dtColumns = [
                    DTColumnBuilder.newColumn('id').withTitle('ID'),
                    DTColumnBuilder.newColumn('created_at').withTitle('Absence Time').notSortable().renderWith(function (data, type, full) {
                        return $filter('date')(data, "dd/MM/yyyy HH:mm");
                    }),
                    DTColumnBuilder.newColumn('user.id').withTitle('User ID'),
                    DTColumnBuilder.newColumn('user.name').withTitle('Name'),
                    DTColumnBuilder.newColumn('absence_type').withTitle('Absence Type'),
                    DTColumnBuilder.newColumn('store.id').withTitle('Store ID'),
                    DTColumnBuilder.newColumn('store.store_uid').withTitle('Store UID'),
                    DTColumnBuilder.newColumn('store.name').withTitle('Store Name'),
                    DTColumnBuilder.newColumn('location').withTitle('Maps').renderWith(function (data) {
                        return ' <a class="btn btn-success btn-small" target="_blank" href="https://www.google.com/maps/?q=' + data + '"> Map </a>';
                    })
                ];
            }
            
            

            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });