angular.module("Dashboard")
        .controller("DownloadVisibilitiesController", function ($window, $scope, Data, $http, $localStorage) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'downloadvisibilities';
                $scope.Data.parent = 'visibilities';

                $scope.waiting = true;
                $scope.success = false;

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/users/list',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.users = data.data.data;
                }, function (data) {

                });

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/stores/list',
                    headers: {
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    $scope.stores = data.data.data;
                }, function (data) {

                });

                $scope.download = function () {
                    if (angular.isUndefined($scope.user_id) || angular.isUndefined($scope.store_id)) {
                        $scope.fail = true;
                        $scope.errmsg = "Please select User ID and Store ID";
                        return false;
                    }
                    $scope.process = true;
                    $scope.fail = false;

                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/visibilities/recap_per_user_store?user_id=' + $scope.user_id + '&store_id=' + $scope.store_id,
                        headers: {
                            'Authorization': $localStorage.jwt
                        },
                    };

                    $http(req).then(function (data) {
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth() + 1;

                        var yyyy = today.getFullYear();
                        if (dd < 10) {
                            dd = '0' + dd;
                        }
                        if (mm < 10) {
                            mm = '0' + mm;
                        }
                        var today = dd + '_' + mm + '_' + yyyy;
                        var anchor = angular.element('<a/>');
                        anchor.css({display: 'none'}); // Make sure it's not visible
                        angular.element(document.body).append(anchor); // Attach to document
                        anchor.attr({
//                            href: req,
                            href: 'https://dell-api.verifi.care/visibilities/recap_per_user_store?user_id=' + $scope.user_id + '&store_id=' + $scope.store_id,
//                            header: 'Authorization:'+$localStorage.jwt,
                            target: '_blank',
                            download: 'visibilities_u' + $scope.user_id + 's' + $scope.store_id + '_' + today + '.zip'
                        })[0].click();



                        anchor.remove();

                        $scope.process = false;
                        $scope.waiting = false;
                        $scope.success = true;
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        }
                        $scope.fail = true;

                        $scope.errmsg = "An error has occurred, please try again later";

                        $scope.waiting = true;
                        $scope.process = false;
                    });

                }
            }

            function injectScript() {
                $('select').select2();
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });