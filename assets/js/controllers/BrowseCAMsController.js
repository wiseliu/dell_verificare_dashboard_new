angular.module("Dashboard")
        .controller("BrowseCAMsController", function ($window, $location, $scope, $http, Data, $localStorage, DTOptionsBuilder, DTColumnBuilder, $q) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'browsecams';
                $scope.Data.parent = 'users';

                var controller = this;
                $scope.level = $localStorage.level;

                $scope.loading = true;

                var vm = this;
                vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    var req = {
                        method: 'GET',
                        url: 'https://dell-api.verifi.care/managers/list',
                        headers: {
                            'Content-Type': "application/x-www-form-urlencoded",
                            'Authorization': $localStorage.jwt
                        }
                    }
                    var defer = $q.defer();
                    $http(req).then(function (data) {
                        controller.response = data.data;

                        $scope.managers = controller.response.data;
                        $scope.loading = false;
                        defer.resolve($scope.managers);
                    }, function (data) {
                        if (data.status === 401) {
                            alert("Your credential is expired or invalid, please log in again");
                            $localStorage.$reset();
                            $window.location.href = 'login.html';
                        } else if (data.status === 404) {
                            $scope.managers = '';
                            $scope.defer.resolve($scope.managers);
                        }else {

                        }
                        $scope.loading = false;
                        $scope.error = true;
                    });
                    return defer.promise;
                }).withBootstrap().withPaginationType('full_numbers').withOption('aaSorting',[]);

                if ($localStorage.level === 'admin') {
                    vm.dtColumns = [
                        DTColumnBuilder.newColumn('id').withTitle('ID'),
                        DTColumnBuilder.newColumn('name').withTitle('Name'),
                        DTColumnBuilder.newColumn('level').withTitle('Level'),
                        DTColumnBuilder.newColumn('id').withTitle('Edit').renderWith(function (full) {
                            return ' <a class="btn btn-success btn-small" href="/users/browse-cams/' + full + '"> Edit </a>';
                        })
                    ];
                } else {
                    vm.dtColumns = [
                        DTColumnBuilder.newColumn('id').withTitle('ID'),
                        DTColumnBuilder.newColumn('name').withTitle('Name'),
                        DTColumnBuilder.newColumn('level').withTitle('Level')
                    ];
                }
            }

            function injectScript() {
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });