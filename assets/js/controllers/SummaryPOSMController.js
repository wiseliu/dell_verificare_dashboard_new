angular.module("Dashboard")
        .controller("SummaryPOSMController", function ($window, $scope, Data, $localStorage, $http) {
            if ($localStorage.level !== 'admin' && $localStorage.level !== 'dashboard') {
                $window.location.href = $window.location.href + '/../';
            } else {
                $scope.Data = Data;
                $scope.Data.link = 'summaryposm';
                $scope.Data.parent = 'reports';

                $scope.loading = true;

                var controller = this;

                var req = {
                    method: 'GET',
                    url: 'https://dell-api.verifi.care/posms/recap_category',
                    headers: {
                        'Content-Type': "application/x-www-form-urlencoded",
                        'Authorization': $localStorage.jwt
                    }
                };

                $http(req).then(function (data) {
                    controller.response = data.data;
                    $scope.oridata = controller.response.data.value;
                    $scope.orilabels = controller.response.data.label;
                    $scope.oritabledata = {data: []};
                    for (var key in $scope.oridata) {
                        $scope.oritabledata.data.push(
                                {
                                    label: $scope.orilabels[key],
                                    value: $scope.oridata[key]
                                }
                        )
                    }

                    $scope.options = {
                        title: {
                            display: true,
                            text: 'Promotional Items Recap'
                        },
                        scales: {
                            yAxes: [{
                                    ticks: {
                                        min: 0,
                                        beginAtZero: true
                                    }
                                }]
                        }
                    }

                    $scope.labels = $scope.orilabels;
                    $scope.tabledata =$scope.oritabledata;
                    $scope.data = $scope.oridata;


                    $scope.loading = false;
                }, function (data) {
                    if (data.status === 401) {
                        alert("Your credential is expired or invalid, please log in again");
                        $localStorage.$reset();
                        $window.location.href = 'login.html';
                    } else if (data.status === 404) {

                    }
                    $scope.loading = false;
                    $scope.error = true;
                });

            }

            function injectScript() {
//                $('select').select2();
            }
            $scope.$on('$viewContentLoaded', function () {
                injectScript();
            });
            $scope.$on('loadJScript', function (event, args) {
                injectScript();
            });
        });