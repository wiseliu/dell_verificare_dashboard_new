var app = angular.module("Dashboard", [
    "ngRoute",
    "ngStorage",
    "angular-jwt",
    'datatables',
    'datatables.bootstrap',
    'chart.js',
    'ngFileUpload',
    'bw.paging'
]);

app.factory('Data', function () {
    return {
        link: '',
        parent: ''
    };
});

//Main Controllers
app.controller("SidebarController", function ($localStorage, $window, $scope, Data) {
    $scope.Data = Data;
    $scope.level = $localStorage.level;
});
app.controller("DashboardController", function ($window, $scope, Data, $localStorage, $location) {
    if (angular.isUndefined($localStorage.jwt)) {
//        $window.location.href = 'login.html';
//        $scope.isLogged = true;
    } else {
//        $scope.isLogged = true;
    }
    $scope.$on('$viewContentLoaded', function () {
    });
    $scope.$on('loadJScript', function (event, args) {
    });
    //console.log('$location', $location.$$absUrl);
});
//Login Controllers
app.controller("LoginController", function ($window, $scope, $http, $localStorage, jwtHelper) {

    $scope.process = false;
    if (!angular.isUndefined($localStorage.sub)) {
        $scope.sub = $localStorage.sub;
        $scope.level = $localStorage.level;
        $scope.displayname = $localStorage.displayname;
        $scope.username = $localStorage.username;
    } else {

    }

    this.logout = function () {
        $localStorage.$reset();
        $window.location.href = 'login.html';
    }

    this.validate = function (user) {
        $scope.process = true;
        if (angular.isUndefined(user)) {

            $scope.process = false;
            $scope.error_message = "Please fill all the fields";
            return false;
        }
        this.user = user;
        var controller = this;
        //console.log("Validating");



        var req = {
            method: 'POST',
            url: 'https://dell-api.verifi.care/login',
//            url: 'https://dell-verificare.herokuapp.com/login',
            dataType: 'json',
            headers: {
                'Content-Type': "application/json"
            },
            data:
                    {
                        "auth":
                                {
                                    "username": controller.user.username,
                                    "password_digest": controller.user.password
                                }
                    }
        }



        controller.response = {
            status: "",
            message: ""
        };
        $http(req).then(function (data) {
            $scope.process = false;
            controller.data = data.data;
            //console.log(jwtHelper.decodeToken(controller.data.jwt));


            if (data.data.jwt !== "") {
                var jwt = jwtHelper.decodeToken(controller.data.jwt);
                $localStorage.sub = jwt.sub;
                $localStorage.level = jwt.level;
                $localStorage.displayname = jwt.name;
                $localStorage.username = jwt.username;
                $localStorage.jwt = controller.data.jwt;
                //console.log(jwt);
                $window.location.href = $window.location.href + '/..';
            }
        }, function (data) {
            $scope.process = false;
            controller.response = data.data;
            if (data.status === 404) {
                $scope.error_message = "The Username or Password is invalid";
            } else {
                $scope.error_message = "An error occurs, cannot login right now, please try again later.";
            }
        });
    }
});
app.config(function ($routeProvider, $locationProvider) {
    if (true) {

    }
    $routeProvider
            .when("/dashboard/", {
                templateUrl: "assets/templates/pages/reports/summary/selloutcam.html",
                controller: "SummarySelloutCamController",
                controllerAs: "Scam"
            })

            .when("/reports/summary/sellout-per-area-manager", {
                templateUrl: "assets/templates/pages/reports/summary/selloutcam.html",
                controller: "SummarySelloutCamController",
                controllerAs: "Scam"
            })
            .when("/reports/summary/sellout-per-area-manager-monthly", {
                templateUrl: "assets/templates/pages/reports/summary/selloutcammonthly.html",
                controller: "SummaryMonthlySelloutCamController",
                controllerAs: "Scam"
            })
            .when("/reports/summary/sellout-per-sku", {
                templateUrl: "assets/templates/pages/reports/summary/selloutsku.html",
                controller: "SummarySelloutSKUController"
            })
            .when("/reports/summary/sellout-per-sku-best", {
                templateUrl: "assets/templates/pages/reports/summary/selloutskubest.html",
                controller: "SummaryBestSKUController"
            })
            .when("/reports/summary/sellout-per-sku-worst", {
                templateUrl: "assets/templates/pages/reports/summary/selloutskuworst.html",
                controller: "SummaryWorstSKUController"
            })
            .when("/reports/summary/sellout-per-region", {
                templateUrl: "assets/templates/pages/reports/summary/selloutregion.html",
                controller: "SummarySelloutRegionController"
            })
            .when("/reports/summary/sellout-per-region-monthly", {
                templateUrl: "assets/templates/pages/reports/summary/selloutregionmonthly.html",
                controller: "SummaryMonthlySelloutRegionController"
            })
            .when("/reports/summary/inventory-per-area-manager", {
                templateUrl: "assets/templates/pages/reports/summary/inventorycam.html",
                controller: "SummaryInventoryCamController"
            })
            .when("/reports/summary/promotional-item-recap", {
                templateUrl: "assets/templates/pages/reports/summary/posm.html",
                controller: "SummaryPOSMController"
            })
            .when("/reports/summary/sku-by-price-category", {
                templateUrl: "assets/templates/pages/reports/summary/skuprice.html",
                controller: "SummarySKUPriceController"
            })

            .when("/reports/absence", {
                templateUrl: "assets/templates/pages/reports/absence/index.html",
                controller: "AbsenceController",
                controllerAs: "Absences"
            })
            .when("/reports/absence/page/:page", {
                templateUrl: "assets/templates/pages/reports/absence/index.html",
                controller: "AbsenceController",
                controllerAs: "Absences"
            })
            
            .when("/reports/my_absence", {
                templateUrl: "assets/templates/pages/reports/absence/index.html",
                controller: "AbsenceSelfController",
                controllerAs: "Absences"
            })
            
            .when("/reports/download-absence", {
                templateUrl: "assets/templates/pages/reports/absence/download-absence.html",
                controller: "DownloadAbsenceController"
            })
            .when("/reports/competitor-issues", {
                templateUrl: "assets/templates/pages/reports/competitor-issues/index.html",
                controller: "CompetitorIssuesController",
                controllerAs: "Competitor"
            })

            //Visibilities
            .when("/visibilities/add-posm", {
                templateUrl: "assets/templates/pages/visibilities/add-posm.html",
                controller: "AddPOSMController"
            })
            .when("/visibilities/browse-posms", {
                templateUrl: "assets/templates/pages/visibilities/browse-posms.html",
                controller: "BrowsePOSMsController",
                controllerAs: "POSM"
            })
            .when("/visibilities/browse-posms/:posm_id", {
                templateUrl: "assets/templates/pages/visibilities/edit-posms.html",
                controller: "EditPOSMsController",
                controllerAs: "POSM"
            })
            .when("/visibilities/browse-posm-inventories", {
                templateUrl: "assets/templates/pages/visibilities/browse-posm-inventories.html",
                controller: "BrowsePOSMInventoriesController",
                controllerAs: "POSM"
            })
            .when("/visibilities/download-posm-inventories", {
                templateUrl: "assets/templates/pages/visibilities/download-posm-inventories.html",
                controller: "DownloadPOSMInventoriesController"
            })
            .when("/visibilities/browse-visibilities", {
                templateUrl: "assets/templates/pages/visibilities/browse-visibilities.html",
                controller: "BrowseVisibilitiesController",
                controllerAs: "Visibilities"
            })
            .when("/visibilities/search-visibility/", {
                templateUrl: "assets/templates/pages/visibilities/search-visibility.html",
                controller: "SearchVisibilityController",
                controllerAs: "Visibility"
            })
            .when("/visibilities/search-visibility/:user_id/:store_id", {
                templateUrl: "assets/templates/pages/visibilities/search-visibility.html",
                controller: "SearchVisibilityController",
                controllerAs: "Visibility"
            })
            .when("/visibilities/download-visibilities", {
                templateUrl: "assets/templates/pages/visibilities/download-visibilities.html",
                controller: "DownloadVisibilitiesController"
            })


            //Stocks
            .when("/stocks/add-inventory", {
                templateUrl: "assets/templates/pages/stocks/add-inventory.html",
                controller: "AddInventoryController"
            })
            .when("/stocks/upload-inventory", {
                templateUrl: "assets/templates/pages/stocks/upload-inventory.html",
                controller: "UploadInventoryController"
            })
            .when("/stocks/download-inventory", {
                templateUrl: "assets/templates/pages/stocks/download-inventory.html",
                controller: "DownloadInventoryController"
            })
            .when("/stocks/browse-inventories", {
                templateUrl: "assets/templates/pages/stocks/browse-inventories.html",
                controller: "BrowseInventoriesController",
                controllerAs: "Inventories"
            })
            .when("/stocks/browse-inventories/page/:page", {
                templateUrl: "assets/templates/pages/stocks/browse-inventories.html",
                controller: "BrowseInventoriesController",
                controllerAs: "Inventories"
            })
            .when("/stocks/browse-inventories-self", {
                templateUrl: "assets/templates/pages/stocks/browse-inventories.html",
                controller: "BrowseInventoriesSelfController",
                controllerAs: "Inventories"
            })
            .when("/stocks/browse-inventories-self/page/:page", {
                templateUrl: "assets/templates/pages/stocks/browse-inventories.html",
                controller: "BrowseInventoriesSelfController",
                controllerAs: "Inventories"
            })
            .when("/stocks/search-inventory", {
                templateUrl: "assets/templates/pages/stocks/search-inventory.html",
                controller: "SearchInventoryController",
                controllerAs: "Servtag"
            })
            .when("/stocks/search-inventory/:filter", {
                templateUrl: "assets/templates/pages/stocks/search-inventory.html",
                controller: "SearchInventoryController",
                controllerAs: "Servtag"
            })

            //Sell In
            .when("/sellins/upload-sell-in", {
                templateUrl: "assets/templates/pages/sellins/upload-sell-in.html",
                controller: "UploadSellInController"
            })
            .when("/sellins/browse-sell-in", {
                templateUrl: "assets/templates/pages/sellins/browse-sell-in.html",
                controller: "BrowseSellInController",
                controllerAs: "Sellin"
            })
            .when("/sellins/browse-sell-in/page/:page", {
                templateUrl: "assets/templates/pages/sellins/browse-sell-in.html",
                controller: "BrowseSellInController",
                controllerAs: "Sellin"
            })
            .when("/sellins/download-sell-in", {
                templateUrl: "assets/templates/pages/sellins/download-sell-in.html",
                controller: "DownloadSellInController"
            })
            .when("/sellins/search-sell-in", {
                templateUrl: "assets/templates/pages/sellins/search-sell-in.html",
                controller: "SearchSellInController",
                controllerAs: "Servtag"
            })
            .when("/sellins/search-sell-in/:filter", {
                templateUrl: "assets/templates/pages/sellins/search-sell-in.html",
                controller: "SearchSellInController",
                controllerAs: "Servtag"
            })

            //Sellout
            .when("/sellouts/create-sell-out", {
                templateUrl: "assets/templates/pages/sellouts/create-sell-out.html",
                controller: "CreateSellOutController"
            })
            .when("/sellouts/upload-sell-out", {
                templateUrl: "assets/templates/pages/sellouts/upload-sell-out.html",
                controller: "UploadSellOutController"
            })
            .when("/sellouts/download-sell-out", {
                templateUrl: "assets/templates/pages/sellouts/download-sell-out.html",
                controller: "DownloadSellOutController"
            })
            .when("/sellouts/download-sell-out-report", {
                templateUrl: "assets/templates/pages/sellouts/download-sell-out-report.html",
                controller: "DownloadSellOutReportController"
            })
            .when("/sellouts/browse-sell-out", {
                templateUrl: "assets/templates/pages/sellouts/browse-sell-out.html",
                controller: "BrowseSellOutController",
                controllerAs: "Sellout"
            })
            .when("/sellouts/browse-sell-out/page/:page", {
                templateUrl: "assets/templates/pages/sellouts/browse-sell-out.html",
                controller: "BrowseSellOutController",
                controllerAs: "Sellout"
            })
            .when("/sellouts/browse-sell-out-self", {
                templateUrl: "assets/templates/pages/sellouts/browse-sell-out.html",
                controller: "BrowseSellOutSelfController",
                controllerAs: "Sellout"
            })
            .when("/sellouts/browse-sell-out-self/page/:page", {
                templateUrl: "assets/templates/pages/sellouts/browse-sell-out.html",
                controller: "BrowseSellOutSelfController",
                controllerAs: "Sellout"
            })
            .when("/sellouts/browse-sell-out/:sellout_id", {
                templateUrl: "assets/templates/pages/sellouts/edit-sell-out.html",
                controller: "EditSellOutController",
                controllerAs: "Sellout"
            })
            .when("/sellouts/search-sell-out", {
                templateUrl: "assets/templates/pages/sellouts/search-sell-out.html",
                controller: "SearchSellOutController",
                controllerAs: "Servtag"
            })
            .when("/sellouts/search-sell-out/:filter", {
                templateUrl: "assets/templates/pages/sellouts/search-sell-out.html",
                controller: "SearchSellOutController",
                controllerAs: "Servtag"
            })

            //Users
            .when("/users/add-cam", {
                templateUrl: "assets/templates/pages/users/add-cam.html",
                controller: "AddCAMController"
            })
            .when("/users/add-user", {
                templateUrl: "assets/templates/pages/users/add-user.html",
                controller: "AddUserController"
            })
            .when("/users/download-user", {
                templateUrl: "assets/templates/pages/users/download-user.html",
                controller: "DownloadUserController"
            })
            .when("/users/download-cam", {
                templateUrl: "assets/templates/pages/users/download-cam.html",
                controller: "DownloadCAMController"
            })
            .when("/users/upload-user", {
                templateUrl: "assets/templates/pages/users/upload-user.html",
                controller: "UploadUserController"
            })
            .when("/users/browse-users", {
                templateUrl: "assets/templates/pages/users/browse-users.html",
                controller: "BrowseUsersController",
                controllerAs: "Users",
            })
            .when("/users/browse-cams", {
                templateUrl: "assets/templates/pages/users/browse-cams.html",
                controller: "BrowseCAMsController",
                controllerAs: "CAM",
            })

            .when("/users/browse-users/:username", {
                templateUrl: "assets/templates/pages/users/edit-user.html",
                controller: "EditUsersController"
            })
            .when("/users/browse-cams/:manager_id", {
                templateUrl: "assets/templates/pages/users/edit-cam.html",
                controller: "EditCAMController"
            })

            //Stores
            .when("/stores/add-store", {
                templateUrl: "assets/templates/pages/stores/add-store.html",
                controller: "AddStoreController"
            })
            .when("/stores/upload-store", {
                templateUrl: "assets/templates/pages/stores/upload-store.html",
                controller: "UploadStoreController"
            })
            .when("/stores/browse-distributors", {
                templateUrl: "assets/templates/pages/stores/browse-distributors.html",
                controller: "BrowseDistributorsController",
                controllerAs: "Distributors"
            })
            .when("/stores/browse-master-dealers", {
                templateUrl: "assets/templates/pages/stores/browse-master-dealers.html",
                controller: "BrowseMasterDealersController",
                controllerAs: "Mdealer"
            })
            .when("/stores/browse-stores", {
                templateUrl: "assets/templates/pages/stores/browse-stores.html",
                controller: "BrowseStoresController",
                controllerAs: "Store"
            })
            .when("/stores/browse-stores/:store_id", {
                templateUrl: "assets/templates/pages/stores/edit-store.html",
                controller: "EditStoresController"
            })
            .when("/stores/download-store", {
                templateUrl: "assets/templates/pages/stores/download-store.html",
                controller: "DownloadStoreController"
            })

            //Areas
            .when("/areas/add-area", {
                templateUrl: "assets/templates/pages/areas/add-area.html",
                controller: "AddAreaController"
            })
            .when("/areas/upload-area", {
                templateUrl: "assets/templates/pages/areas/upload-area.html",
                controller: "UploadAreaController"
            })
            .when("/areas/browse-areas", {
                templateUrl: "assets/templates/pages/areas/browse-areas.html",
                controller: "BrowseAreasController",
                controllerAs: "Area"
            })
            .when("/areas/download-areas", {
                templateUrl: "assets/templates/pages/areas/download-areas.html",
                controller: "DownloadAreasController"
            })
            .when("/areas/browse-regions", {
                templateUrl: "assets/templates/pages/areas/browse-regions.html",
                controller: "BrowseRegionsController",
                controllerAs: "Regions"
            })
            .when("/areas/download-regions", {
                templateUrl: "assets/templates/pages/areas/download-regions.html",
                controller: "DownloadRegionsController"
            })

            //Posts
            .when("/posts/create-post", {
                templateUrl: "assets/templates/pages/posts/create-post.html",
                controller: "CreatePostController"
            })
            .when("/posts/browse-posts", {
                templateUrl: "assets/templates/pages/posts/browse-posts.html",
                controller: "BrowsePostsController",
                controllerAs: "Post"
            })
            .when("/posts/browse-posts/:post_id", {
                templateUrl: "assets/templates/pages/posts/browse-single-post.html",
                controller: "BrowseSinglePostController",
                controllerAs: "Post"
            })
            .when("/posts/browse-comments", {
                templateUrl: "assets/templates/pages/posts/browse-comments.html",
                controller: "BrowseCommentsController",
                controllerAs: "Comments"
            })

            //Sells Kit
            .when("/sell-kits/create-sell-kit", {
                templateUrl: "assets/templates/pages/sell-kits/create-sell-kit.html",
                controller: "CreateSellKitController"
            })
            .when("/sell-kits/browse-sell-kits", {
                templateUrl: "assets/templates/pages/sell-kits/browse-sell-kits.html",
                controller: "BrowseSellKitsController",
                controllerAs: "SKits"
            })

//            Redirect to Summary Page if not found
            .otherwise({
                redirectTo: "/dashboard"
            })
    $locationProvider.html5Mode(true);
});
app.run(['$rootScope', '$location', '$localStorage', '$window', function ($rootScope, $location, $localStorage, $window) {
        $rootScope.$on('$routeChangeStart', function () {

            //check if the user is going to the login page
            // i use ui.route so not exactly sure about this one but you get the picture
//        var appTo = currRoute.path.indexOf('/dashboard') !== -1;

            if (angular.isUndefined($localStorage.jwt) && $location.$$path !== "/login.html") {
                $window.location.href = 'login.html';
            }
        });
    }]);
app.directive('jqueryScript', function () {
    return {
        restrict: 'E',
        link: function (rootScope, $scope, $element, $attrs) {
            $(document).ready(function () {
                $.getScript("assets/plugins/pace/pace.min.js", function (data, textStatus, jqxhr) {
                    //console.log("Load pace.min.js");
                    $.getScript("assets/plugins/jquery-block-ui/jqueryblockui.min.js", function (data, textStatus, jqxhr) {
                        //console.log("Load jqueryblockui.min.js");
                        $.getScript("assets/plugins/jquery-unveil/jquery.unveil.min.js", function (data, textStatus, jqxhr) {
                            //console.log("Load jquery.unveil.min.js");
                            $.getScript("assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js", function (data, textStatus, jqxhr) {
                                //console.log("Load jquery.scrollbar.min.js");
                                $.getScript("assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js", function (data, textStatus, jqxhr) {
                                    //console.log("Load jquery.animateNumbers.js");
                                    $.getScript("assets/plugins/jquery-validation/js/jquery.validate.min.js", function (data, textStatus, jqxhr) {
                                        //console.log("Load jquery.validate.min.js");
                                        $.getScript("assets/plugins/bootstrap-select2/select2.min.js", function (data, textStatus, jqxhr) {
                                            //console.log("Load select2.min.js");
                                            $.getScript("webarch/js/webarch.js", function (data, textStatus, jqxhr) {
                                                //console.log("Load webarch.js");
                                                $.getScript("assets/js/chat.js", function (data, textStatus, jqxhr) {
                                                    //console.log("Load chat.js");
                                                    $.getScript("assets/js/stacktable.js", function (data, textStatus, jqxhr) {
                                                        //console.log("Load Stacktable.js");
                                                        $.getScript("assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js", function (data, textStatus, jqxhr) {
                                                            //console.log("Load Bootstrap Datepicker.js");
                                                            $.getScript("assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js", function (data, textStatus, jqxhr) {
                                                                //console.log("Load Bootstrap Timepicker.js");
                                                                $.getScript("assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js", function (data, textStatus, jqxhr) {
                                                                    //console.log("Load Bootstrap ColorPicker.js");
                                                                    $.getScript("assets/plugins/jquery-inputmask/jquery.inputmask.min.js", function (data, textStatus, jqxhr) {
                                                                        //console.log("Load input mask.js");
                                                                        $.getScript("assets/plugins/jquery-autonumeric/autoNumeric.js", function (data, textStatus, jqxhr) {
                                                                            //console.log("Load Autonumeric.js");
                                                                            $.getScript("assets/plugins/ios-switch/ios7-switch.js", function (data, textStatus, jqxhr) {
                                                                                //console.log("Load IOS7 Switch.js");
                                                                                $.getScript("assets/plugins/bootstrap-select2/select2.min.js", function (data, textStatus, jqxhr) {
                                                                                    //console.log("Load Bootstrap Select2.js");
                                                                                    $.getScript("assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js", function (data, textStatus, jqxhr) {
                                                                                        //console.log("Load Bootstrap wysihtml5.js");
                                                                                        $.getScript("assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js", function (data, textStatus, jqxhr) {
                                                                                            //console.log("Load Bootstrap wysihtml5 2.js");
                                                                                            $.getScript("assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js", function (data, textStatus, jqxhr) {
                                                                                                //console.log("Load Bootstrap tagsinput.js");
                                                                                                $.getScript("assets/plugins/boostrap-clockpicker/bootstrap-clockpicker.min.js", function (data, textStatus, jqxhr) {
                                                                                                    //console.log("Load Bootstrap clockpicker.js");
                                                                                                    $.getScript("assets/plugins/dropzone/dropzone.js", function (data, textStatus, jqxhr) {
                                                                                                        //console.log("Load Bootstrap dropzone.js");
                                                                                                        rootScope.$broadcast('loadJScript');
                                                                                                    });
                                                                                                });
                                                                                            });
                                                                                        });
                                                                                    });
                                                                                });
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
//                                });
                            });
                        });
                    });
                });
            });
        },
    };
});
app.directive('dashboardHeader', function () {
    return{
        restrict: 'E',
        templateUrl: 'assets/templates/parts/header/main.html'
    };
});
app.directive('dashboardSidebar', function () {
    return{
        restrict: 'E',
        templateUrl: 'assets/templates/parts/sidebar/main.html'
    };
});